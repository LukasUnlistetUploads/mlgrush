package de.primeapi.mlgrush;

import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.api.Setup;
import de.primeapi.mlgrush.commands.*;
import de.primeapi.mlgrush.commands.tabcompletion.SpecCompleter;
import de.primeapi.mlgrush.config.MapsConfig;
import de.primeapi.mlgrush.config.SettingsConfig;
import de.primeapi.mlgrush.lcn.LCN;
import de.primeapi.mlgrush.listeners.GameListeners;
import de.primeapi.mlgrush.listeners.GeneralListeners;
import de.primeapi.mlgrush.listeners.LobbyListeners;
import de.primeapi.mlgrush.managers.InventoryManager;
import de.primeapi.mlgrush.managers.MapManager;
import de.primeapi.mlgrush.managers.messages.MessageManager;
import de.primeapi.mlgrush.sql.MySQL;
import de.primeapi.mlgrush.sql.SQLPlayer;
import de.primeapi.mlgrush.utilities.guiapi.GUIManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class MLGRush extends JavaPlugin {

    private static MLGRush instance;
    private ThreadPoolExecutor threadPoolExecutor;
    private HashMap<UUID, RushPlayer> playerCache;
    private static MessageManager messageManager;
    private String prefix;
    public Setup setup;
    public boolean license;

    @Override
    public void onEnable() {
        instance = this;
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
        Bukkit.getConsoleSender().sendMessage("§8§m--------------------------");
        Bukkit.getConsoleSender().sendMessage("§7Plugin §eMLGRush");
        Bukkit.getConsoleSender().sendMessage("§7Version §e" + getDescription().getVersion());
        Bukkit.getConsoleSender().sendMessage("§7Author §ePrimeAPI");
        Bukkit.getConsoleSender().sendMessage("§8§m--------------------------");
        playerCache = new HashMap<>();

        try {
            SettingsConfig.init();
            MapsConfig.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
        license = new LCN(SettingsConfig.cfg.getString("settings.license"), "http://primeapi.de/shop/licence/verify.php").register();
        if(!license){
            Bukkit.getPluginManager().registerEvents(new org.bukkit.event.Listener(){
                @EventHandler
                public void onJOin(PlayerLoginEvent e){
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§cLicense in invalid!");
                }
            },this);
        }

        prefix = SettingsConfig.cfg.getString("settings.prefix");
        try {
            messageManager = new MessageManager();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MySQL.connect(SettingsConfig.cfg.getString("mysql.host"),SettingsConfig.cfg.getString("mysql.database"),SettingsConfig.cfg.getString("mysql.username"),SettingsConfig.cfg.getString("mysql.password"));
        MySQL.setup();
        MapManager.init();
        getCommand("setup").setExecutor(new SetupCommand());
        getCommand("c").setExecutor(new CCommand());
        getCommand("debug").setExecutor(new DebugCommand());
        getCommand("setqueue").setExecutor(new SetQueueCommand());
        getCommand("quit").setExecutor(new LeaveCommand());
        getCommand("stats").setExecutor(new StatsCommand());
        getCommand("toggleinvites").setExecutor(new ToggleInvitesCommand());
        getCommand("setstatswall").setExecutor(new SetStatsWallCommand());
        getCommand("spec").setExecutor(new SpecCommand());
        getCommand("spec").setTabCompleter(new SpecCompleter());
        getCommand("mlgrush").setExecutor(new CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
                commandSender.sendMessage("MLGRush v" + getDescription().getVersion() + " by PrimeAPI");
                commandSender.sendMessage("primeapi.de");
                return true;
            }
        });
        getCommand("build").setExecutor(new CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
                Player p = (Player) commandSender;
                if(!p.hasPermission("mlgrush.build")) return false;
                if(p.getGameMode() != GameMode.CREATIVE){
                    p.setGameMode(GameMode.CREATIVE);
                    p.sendMessage("§7Du hast den Bau-Modus betreten");
                }else {
                    p.setGameMode(GameMode.SURVIVAL);
                    p.sendMessage("§7Du hast den Bau-Modus verlassen");
                    InventoryManager.setHotbar(getPlayer(p.getUniqueId()));
                }
                return true;
            }
        });


        Bukkit.getPluginManager().registerEvents(new GeneralListeners(), this);
        Bukkit.getPluginManager().registerEvents(new LobbyListeners(), this);
        Bukkit.getPluginManager().registerEvents(new GameListeners(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryManager(), this);
        Bukkit.getPluginManager().registerEvents(new GUIManager(), this);

        for(World w :Bukkit.getWorlds()){
            w.setGameRuleValue("doMobSpawning", "false");
            w.setGameRuleValue("doDaylightCycle", "false");
        }

        if(SettingsConfig.cfg.getBoolean("settings.statswall.use")) {
            setupStats();
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::setupStats, 15*60*20, 15*60*20);

        }


    }

    @Override
    public void onDisable() {
        MySQL.disconnect();
        for (Player all : Bukkit.getOnlinePlayers()){
            all.kickPlayer("§cServer startet Neu!");
        }
    }

    public ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }

    public HashMap<UUID, RushPlayer> getPlayerCache() {
        return playerCache;
    }

    public String getPrefix() {
        return prefix;
    }

    public RushPlayer getPlayer(UUID uuid){
        return new RushPlayer(Bukkit.getPlayer(uuid));
    }

    public void unregisterPlayer(UUID uuid){
        playerCache.remove(uuid);
    }

    public static MLGRush getInstance() {
        return instance;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        System.out.println("command.getName() = " + command.getName());
        System.out.println("alias = " + alias);
        System.out.println("args = " + Arrays.toString(args));
        return super.onTabComplete(sender, command, alias, args);
    }

    public void setupStats(){
            List<UUID> list = MySQL.getAllPlayers();
            int amount = 0;
            for (int i = 1; i <= SettingsConfig.cfg.getInt("settings.statswall.amount"); i++){
                try {
                    Location skullLocation = MapsConfig.getLocation("statswall." + i + ".head");
                    Location signLocation = MapsConfig.getLocation("statswall." + i + ".sign");
                    Skull skull = (Skull) skullLocation.getBlock().getState();
                    Sign sign = (Sign) signLocation.getBlock().getState();
                    if (i > list.size()) {
                        skull.setOwner("MHF_Question");
                        skull.update();
                        sign.setLine(0, "- Platz #" + i + " -");
                        sign.setLine(1, "???");
                        sign.setLine(2, "??? Wins");
                        sign.setLine(3, "??? Beds");
                        sign.update();
                        continue;
                    }
                    SQLPlayer player = new SQLPlayer(list.get(i - 1));
                    skull.setOwner(player.getRealname());
                    skull.update();
                    sign.setLine(0, "- Platz #" + i + " -");
                    sign.setLine(1, player.getRealname());
                    sign.setLine(2, player.getWins() + " Wins");
                    sign.setLine(3, player.getBeds() + " Beds");
                    sign.update();
                    amount = i;
                } catch (Exception ex) {
                    System.out.println("[MLGRush] Es wurden " + amount + " Stats-Positionen geladen!");
                    break;
                }
                System.out.println("[MLGRush] Es wurden " + amount + " Stats-Positionen geladen!");
            }
    }



}
