package de.primeapi.mlgrush.api;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.commands.SpecCommand;
import de.primeapi.mlgrush.config.MapsConfig;
import de.primeapi.mlgrush.managers.*;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Duels {

    public static HashMap<UUID, Integer> map = new HashMap<>();
    public static HashMap<UUID, UUID> redPlayer = new HashMap<>();
    public static HashMap<UUID, UUID> bluePlayer = new HashMap<>();
    public static HashMap<UUID, Integer> redScore = new HashMap<>();
    public static HashMap<UUID, Integer> blueScore = new HashMap<>();
    public static HashMap<UUID, Thread> animation = new HashMap<>();
    public static HashMap<UUID, ArrayList<Location>> placedBlocks = new HashMap<>();
    public static HashMap<UUID, Boolean> wait = new HashMap<>();



    public static Integer getMap(UUID uuid) {
        return map.get(uuid);
    }
    public static UUID getRedPlayer(UUID uuid) {
        return redPlayer.get(uuid);
    }
    public static UUID getBluePlayer(UUID uuid) {
        return bluePlayer.get(uuid);
    }
    public static Integer getRedScore(UUID uuid) {
        return redScore.get(uuid);
    }
    public static Integer getBlueScore(UUID uuid) {
        return blueScore.get(uuid);
    }
    public static Thread getAnimation(UUID uuid) {
        return animation.get(uuid);
    }
    public static boolean getWait(UUID uuid) {
        return wait.get(uuid);
    }

    public static ArrayList<Location> getPlacedBlocks(UUID uuid) {
        return placedBlocks.get(uuid);
    }

    public static void setMap(UUID uuid, Integer i) {
        map.put(uuid, i);
        map.put(getOpponent(uuid), i);
    }
    public static void setRedPlayer(UUID uuid, UUID uuid1) {
        redPlayer.put(uuid, uuid1);
        redPlayer.put(getOpponent(uuid), uuid1);
    }
    public static void setBluePlayer(UUID uuid, UUID uuid1) {
        bluePlayer.put(uuid, uuid1);
        bluePlayer.put(getOpponent(uuid), uuid1);
    }
    public static void setRedScore(UUID uuid, Integer i) {
        redScore.put(uuid, i);
        redScore.put(getOpponent(uuid), i);
    }
    public static void setBlueScore(UUID uuid, Integer i) {
        blueScore.put(uuid, i);
        blueScore.put(getOpponent(uuid), i);
    }
    public static void setAnimation(UUID uuid, Thread t) {
        animation.put(uuid, t);
        animation.put(getOpponent(uuid), t);
    }
    public static void setPlacedBlocks(UUID uuid, ArrayList<Location> placedBlocks) {
        Duels.placedBlocks.put(uuid,placedBlocks);
        Duels.placedBlocks.put(getOpponent(uuid),placedBlocks);
    }
    public static void setWaituuid(UUID uuid, boolean b) {
        Duels.wait.put(uuid,b);
        Duels.wait.put(getOpponent(uuid),b);
    }

    public static void addPlacedBlock(UUID uuid, Location location){
        ArrayList<Location> list = getPlacedBlocks(uuid);
        list.add(location);
        setPlacedBlocks(uuid, list);
    }
    public static void removePlacedBlock(UUID uuid, Location location){
        ArrayList<Location> list = getPlacedBlocks(uuid);
        list.remove(location);
        setPlacedBlocks(uuid, list);
    }
    public static void clearPlacedBlock(UUID uuid){
        ArrayList<Location> list = new ArrayList<>();
        setPlacedBlocks(uuid, list);
    }

    public static void addScoreBlue(UUID uuid, int i){
        setBlueScore(uuid, getBlueScore(uuid) + i);
    }
    public static void addScoreRed(UUID uuid, int i){
        setRedScore(uuid, getRedScore(uuid) + i);
    }

    public static void start(RushPlayer p, RushPlayer p1){
        DuelManagers.waiting.remove(p.getUuid());
        DuelManagers.waiting.remove(p1.getUuid());
        for(Player all : Bukkit.getOnlinePlayers()){
            if(!all.getUniqueId().equals(p.getUuid()) && !all.getUniqueId().equals(p1.getUuid())){
                all.hidePlayer(p.thePlayer());
                all.hidePlayer(p1.thePlayer());
                p.thePlayer().hidePlayer(all);
                p1.thePlayer().hidePlayer(all);
            }
        }
        if(MapManager.availibeMap.size() <= 0){
            DuelManagers.waiting.put(p.getUuid(), p1.getUuid());
            DuelManagers.waiting.put(p1.getUuid(), p.getUuid());
            sendAnimation(p, p1);
            return;
        }
        Integer mapId = MapManager.availibeMap.get(0);
        p.thePlayer().teleport(MapsConfig.getLocation("maps." + mapId + ".red.spawn"));
        p1.thePlayer().teleport(MapsConfig.getLocation("maps." + mapId + ".blue.spawn"));
        map.put(p.getUuid(), mapId);
        map.put(p1.getUuid(), mapId);
        MapManager.availibeMap.remove(mapId);
        if(animation.containsKey(p.getUuid())) animation.get(p.getUuid()).stop();
        animation.remove(p.getUuid());
        if(animation.containsKey(p1.getUuid())) animation.get(p1.getUuid()).stop();
        animation.remove(p1.getUuid());

        //p = red p1 = blue
        redPlayer.put(p.getUuid(), p.getUuid());
        redPlayer.put(p1.getUuid(), p.getUuid());

        bluePlayer.put(p.getUuid(), p1.getUuid());
        bluePlayer.put(p1.getUuid(), p1.getUuid());

        blueScore.put(p.getUuid(), 0);
        blueScore.put(p1.getUuid(), 0);
        redScore.put(p.getUuid(), 0);
        redScore.put(p1.getUuid(), 0);

        placedBlocks.put(p.getUuid(), new ArrayList<>());
        placedBlocks.put(p1.getUuid(), new ArrayList<>());

        ScoreboardManager.sendIngame(p);
        ScoreboardManager.sendIngame(p1);
        p.sendMessage(Message.DUELL_START_1.replace("player", p1.getName()));
        p1.sendMessage(Message.DUELL_START_1.replace("player", p.getName()));
        p.sendMessage(Message.DUELL_START_2);
        p1.sendMessage(Message.DUELL_START_2);
        QueueManager.requests.remove(p.getUuid());
        QueueManager.requests.remove(p1.getUuid());
        QueueManager.queue.remove(p.getUuid());
        QueueManager.queue.remove(p1.getUuid());
        InventoryManager.setIngame(p);
        InventoryManager.setIngame(p1);
        wait.put(p.getUuid(), true);
        wait.put(p1.getUuid(), true);
        p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
        p1.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
        Bukkit.getScheduler().runTaskLater(MLGRush.getInstance(), () -> {
            setWaituuid(p.getUuid(), false);
        }, 25);
    }

    public static void sendAnimation(RushPlayer p, RushPlayer p1){
       Thread thread = new Thread( () -> {
            while (DuelManagers.waiting.containsKey(p.getUuid()) && DuelManagers.waiting.containsKey(p1.getUuid())){//if not starts loading animation
                try {
                    String sub;
                    int wait = 250;

                    sub = "§a█⬛⬛⬛⬛⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛█⬛⬛⬛⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛█⬛⬛⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛█⬛⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛⬛█⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛⬛⬛█⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛⬛⬛⬛█⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛⬛⬛⬛⬛█";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛⬛⬛⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛⬛⬛⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);

                    sub = "§a⬛⬛⬛⬛⬛⬛⬛⬛";
                    TitleAPI.sendTitle(p.p,0, 25, 0, "§7Suche nach Server", sub);
                    TitleAPI.sendTitle(p1.p,0, 25, 0, "§7Suche nach Server", sub);
                    Thread.sleep(wait);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

       thread.start();
       animation.put(p.getUuid(), thread);
       animation.put(p1.getUuid(), thread);
    }

    public static void end(UUID uuid){
        if(map.containsKey(uuid)) {
            MapManager.availibeMap.add(getMap(uuid));
        }
        if(DuelManagers.waiting.size() >= 1){
            RushPlayer p1 = null;
            RushPlayer p2 = null;
            for(UUID u : DuelManagers.waiting.keySet()){
                p1 = MLGRush.getInstance().getPlayer(u);
                p2 = MLGRush.getInstance().getPlayer(DuelManagers.waiting.get(u));
                break;
            }
            Duels.start(p1, p2);
        }
        if(animation.containsKey(uuid)) {
            animation.get(uuid).stop();
        }
        animation.remove(uuid);

        UUID uuid1;
        if(DuelManagers.waiting.containsKey(uuid)){
            uuid1 = DuelManagers.waiting.get(uuid);
        }else {
            uuid1 = getOpponent(uuid);
        }
        if(animation.containsKey(uuid1)){
            animation.get(uuid1).stop();
        }

        DuelManagers.waiting.remove(uuid);
        DuelManagers.waiting.remove(uuid1);

        if(placedBlocks.containsKey(uuid)) {
            for (Location loc : getPlacedBlocks(uuid)) {
                loc.getBlock().setType(Material.AIR);
            }
        }

        animation.remove(uuid1);
        map.remove(uuid);
        map.remove(uuid1);
        redPlayer.remove(uuid);
        redPlayer.remove(uuid1);
        bluePlayer.remove(uuid);
        bluePlayer.remove(uuid1);
        redScore.remove(uuid);
        redScore.remove(uuid1);
        blueScore.remove(uuid);
        blueScore.remove(uuid1);
        placedBlocks.remove(uuid);
        placedBlocks.remove(uuid1);

        RushPlayer p = MLGRush.getInstance().getPlayer(uuid);
        RushPlayer p1 = MLGRush.getInstance().getPlayer(uuid1);
        for(Player allplayer : Bukkit.getOnlinePlayers()){
            RushPlayer all = MLGRush.getInstance().getPlayer(allplayer.getUniqueId());
            if(all.isLobby()){
                all.thePlayer().showPlayer(p.thePlayer());
                all.thePlayer().showPlayer(p1.thePlayer());
                p.thePlayer().showPlayer(all.thePlayer());
                p1.thePlayer().showPlayer(all.thePlayer());
            }
        }
        p.teleportSpawn();
        p1.teleportSpawn();
        ScoreboardManager.sendLobby(p);
        ScoreboardManager.sendLobby(p1);
        InventoryManager.setHotbar(p);
        InventoryManager.setHotbar(p1);
        p.thePlayer().setLevel(0);
        p1.thePlayer().setLevel(0);
        if(SpecCommand.curr.containsValue(p.getUuid())){
            for(UUID uid : SpecCommand.curr.keySet()){
                if(SpecCommand.curr.get(uid).equals(p.getUuid())){
                    RushPlayer p2 = MLGRush.getInstance().getPlayer(uid);
                    p2.sendMessage(Message.DUELL_END);
                    SpecCommand.stopSpec(p2);
                }
            }
        }
        if(SpecCommand.curr.containsValue(p1.getUuid())){
            for(UUID uid : SpecCommand.curr.keySet()){
                if(SpecCommand.curr.get(uid).equals(p1.getUuid())){
                    RushPlayer p2 = MLGRush.getInstance().getPlayer(uid);
                    p2.sendMessage(Message.DUELL_END);
                    SpecCommand.stopSpec(p2);
                }
            }
        }
    }

    public static UUID getOpponent(UUID uuid){
        if(getBluePlayer(uuid).toString().equals(uuid.toString())){
            return getRedPlayer(uuid);
        }else if (getRedPlayer(uuid).toString().equals(uuid.toString())){
            return getBluePlayer(uuid);
        }else {
            throw new IllegalArgumentException("Die UUID " + uuid.toString() + " ist in keinem Duel!");
        }
    }

}
