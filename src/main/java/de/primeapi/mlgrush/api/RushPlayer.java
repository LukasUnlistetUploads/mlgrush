package de.primeapi.mlgrush.api;

import com.google.gson.internal.$Gson$Preconditions;
import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.config.MapsConfig;
import de.primeapi.mlgrush.config.SettingsConfig;
import de.primeapi.mlgrush.managers.DuelManagers;
import de.primeapi.mlgrush.managers.messages.Message;
import de.primeapi.mlgrush.sql.SQLPlayer;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.UUID;

public class RushPlayer extends SQLPlayer {

    Player p;

    public RushPlayer(Player p) {
        super(p.getName().toLowerCase(), p.getUniqueId());
        this.p = p;
    }

    public void sendMessage(Message message){
        p.sendMessage(message.getContent());
    }


    @Override
    public String getName() {
        return thePlayer().getName();
    }


    public boolean hasPermission(String s) {
        return p.hasPermission(s);
    }

    public void sendNoPerm() {
        sendMessage(Message.NOPERMS);
    }

    public Player getP() {
        return p;
    }

    public Player getPlayer() {
        return p;
    }

    public Player thePlayer() {
        return p;
    }

    public void teleportSpawn() {
        thePlayer().teleport(MapsConfig.getLocation("spawn"));
    }

    public void playSound(Location location, Sound sound, int i, int i1){
        if(SettingsConfig.cfg.getBoolean("settings.effects.sound")) {
            thePlayer().playSound(location, sound, i, i1);
        }
    }

    public void playSound(Sound sound, int i, int i1){
        playSound(thePlayer().getLocation(), sound, i, i1);
    }

    public void playSound(Sound sound){
        playSound(thePlayer().getLocation(), sound, 1, 1);
    }

    public boolean isLobby(){
        return !Duels.bluePlayer.containsKey(p.getUniqueId());
    }
}
