package de.primeapi.mlgrush.api;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.config.MapsConfig;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;


public class Setup implements Listener {

    public Player player;
    public RushPlayer p;
    public int mapcount;
    public int step;

    public Setup(Player player){
        this.player = player;
        this.p = MLGRush.getInstance().getPlayer(player.getUniqueId());
        for(Player all : Bukkit.getOnlinePlayers()){
            if(!all.getName().equals(player.getName())){
                all.kickPlayer("§cEs wird ein Setup durchgeführt!");
            }
        }
        try {
            MapsConfig.getLocation("spawn");
            step = 0;
            nextStep();
        }catch (Exception exception){
            step = -1;
            nextStep();
        }
        Bukkit.getPluginManager().registerEvents(this, MLGRush.getInstance());
        mapcount = MapsConfig.getMapsAmount() + 1;
    }

    public void end(){
        Bukkit.reload();
    }

    public void nextStep(){
        step++;
        p.thePlayer().setAllowFlight(true);
        switch (step){
            case 0:
                p.sendMessage(Message.SETUP_0);
                break;
            case 1:
                p.sendMessage(Message.SETUP_1);
                break;
            case 2:
                p.sendMessage(Message.SETUP_2);
                break;
            case 3:
                p.sendMessage(Message.SETUP_3);
                break;
            case 4:
                p.sendMessage(Message.SETUP_4);
                break;
            case 5:
                p.sendMessage(Message.SETUP_5);
                break;
            case 6:
                p.sendMessage(Message.SETUP_6);
                break;
            case 7:
                p.sendMessage(Message.SETUP_7_1);
                p.sendMessage(Message.SETUP_7_2);
                break;
            case 8:
                p.sendMessage(Message.SETUP_8_1);
                p.sendMessage(Message.SETUP_8_2);
                break;
            case 9:
                p.sendMessage(Message.SETUP_9);
                MapsConfig.addMap();
                break;
            case 10:
                end();
                break;
        }
    }

    Location pos1;
    Location pos2;


    //Listeners
    @EventHandler
    public void onChat(PlayerChatEvent e){
        String msg = e.getMessage();
        if(msg.equalsIgnoreCase("fertig")){
            e.setCancelled(true);
            switch (step){
                case 0:
                    MapsConfig.saveLocation(p.thePlayer().getLocation(), "spawn");
                    nextStep();
                    break;
                case 1:
                    MapsConfig.saveLocation(p.thePlayer().getLocation(), "maps." + mapcount + ".red.spawn");
                    nextStep();
                    break;
                case 2:
                    MapsConfig.saveLocation(p.thePlayer().getLocation(), "maps." + mapcount + ".blue.spawn");
                    nextStep();
                    break;
                case 7:
                    pos1 = p.thePlayer().getLocation();
                    nextStep();
                    break;
                case 8:
                    pos2 = p.thePlayer().getLocation();
                    Location loc1 = pos1.clone();
                    Location loc2 = pos2.clone();

                    if(pos1.getX() < pos2.getX()){
                        loc1.setX(loc2.getX());
                        loc2.setX(loc1.getX());
                    }
                    if(pos1.getY() < pos2.getY()){
                        loc1.setY(loc2.getY());
                        loc2.setY(loc1.getY());
                    }
                    if(pos1.getZ() < pos2.getZ()){
                        loc1.setZ(loc2.getZ());
                        loc2.setZ(loc1.getZ());
                    }

                    MapsConfig.saveLocation(pos2, "maps." + mapcount + ".region.2");
                    MapsConfig.saveLocation(pos1, "maps." + mapcount + ".region.1");
                    nextStep();
                    break;
                case 9:
                    nextStep();
            }
        }
    }


    @EventHandler
    public void onBlockBread(BlockBreakEvent e){
        e.setCancelled(true);
        switch (step){
            case 3:
                MapsConfig.saveLocation(e.getBlock().getLocation(), "maps." + mapcount + ".red.bed.1");
                nextStep();
                break;
            case 4:
                MapsConfig.saveLocation(e.getBlock().getLocation(), "maps." + mapcount + ".red.bed.2");
                nextStep();
                break;
            case 5:
                MapsConfig.saveLocation(e.getBlock().getLocation(), "maps." + mapcount + ".blue.bed.1");
                nextStep();
                break;
            case 6:
                MapsConfig.saveLocation(e.getBlock().getLocation(), "maps." + mapcount + ".blue.bed.2");
                nextStep();
                break;
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        if(player.getUniqueId() != e.getPlayer().getUniqueId()){
            e.getPlayer().kickPlayer("§cEs läuft aktuell ein Setup!");
        }
    }

}
