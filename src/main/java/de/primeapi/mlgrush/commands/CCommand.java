package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.managers.DuelManagers;
import de.primeapi.mlgrush.managers.QueueManager;
import de.primeapi.mlgrush.managers.ScoreboardManager;
import de.primeapi.mlgrush.managers.messages.Message;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class CCommand implements CommandExecutor {


    public static ArrayList<UUID> cooldown = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }

        RushPlayer p = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());

        if(args.length == 0){
            p.sendMessage(Message.CHALLANGE_USAGE);
            return true;
        }

        if(!p.isLobby()){
            p.sendMessage(Message.CHALLANGE_ALREADY_MATCH);
            return true;
        }

        if(cooldown.contains(p.getUuid())){
            return true;
        }

        cooldown.add(p.getUuid());
        MLGRush.getInstance().getThreadPoolExecutor().submit(() -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cooldown.remove(p.getUuid());
        });


        if(args[0].equalsIgnoreCase("Queue")){
            if (QueueManager.queue.contains(p.getUuid())) {
                p.sendMessage(Message.CHALLANGE_QUEUE_LEAVE);
                p.playSound(p.thePlayer().getLocation(), Sound.NOTE_BASS, 1, 1);
                QueueManager.queue.remove(p.getUuid());
            } else {
                if (QueueManager.requests.containsKey(p.getUuid())) {
                    RushPlayer oldT = MLGRush.getInstance().getPlayer(QueueManager.requests.get(p.getUuid()));
                    oldT.sendMessage(Message.CHALLANGE_CHALLANGE_BACK_NOTIFY.replace("player", p.getName()));
                    p.sendMessage(Message.CHALLANGE_CHALLANGE_BACK_SUCCESS.replace("player", oldT.getName()));
                    p.playSound(p.thePlayer().getLocation(), Sound.NOTE_BASS, 1, 1);
                    QueueManager.requests.remove(p.getUuid());
                }
                p.sendMessage(Message.CHALLANGE_QUEUE_ENTER);
                p.playSound(p.thePlayer().getLocation(), Sound.ORB_PICKUP, 1, 1);

                if (QueueManager.queue.size() >= 1) {
                    RushPlayer p1 = MLGRush.getInstance().getPlayer(QueueManager.queue.get(0));
                    QueueManager.queue.remove(p1.getUuid());
                    Duels.start(p, p1);
                } else {
                    QueueManager.queue.add(p.getUuid());
                }
            }
            ScoreboardManager.sendLobby(p);
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if(target == null){
            p.sendMessage(Message.CHALLANGE_NOTFOUND);
            return true;
        }
        if(!p.isLobby() || DuelManagers.waiting.containsKey(p.getUuid())){
            p.sendMessage(Message.CHALLANGE_ALREADY_MATCH);
            return true;
        }

        RushPlayer t = MLGRush.getInstance().getPlayer(target.getUniqueId());



        if(!t.isLobby() || DuelManagers.waiting.containsKey(t.getUuid())){
            p.sendMessage(Message.CHALLANGE_ALREAY_OTHER);
            return true;
        }
        if(p.getUuid().equals(t.getUuid())){
            p.sendMessage(Message.CHALLANGE_SELF);
            return true;
        }


        if(QueueManager.queue.contains(p.getUuid())){
            p.sendMessage(Message.CHALLANGE_QUEUE_LEAVE);
            p.playSound(p.thePlayer().getLocation(), Sound.NOTE_BASS, 1, 1);
            QueueManager.queue.remove(p.getUuid());
        }

        if(QueueManager.requests.containsKey(t.getUuid())){
            Duels.start(p, t);
            return true;
        }


        if(QueueManager.requests.containsKey(p.getUuid())){
            RushPlayer oldT = MLGRush.getInstance().getPlayer(QueueManager.requests.get(p.getUuid()));
            oldT.sendMessage(Message.CHALLANGE_CHALLANGE_BACK_NOTIFY.replace("player", p.getName()));
            p.sendMessage(Message.CHALLANGE_CHALLANGE_BACK_SUCCESS.replace("player", oldT.getName()));
            p.playSound(p.thePlayer().getLocation(), Sound.NOTE_BASS, 1, 1);
            oldT.playSound(p.thePlayer().getLocation(), Sound.NOTE_BASS, 1, 1);
            QueueManager.requests.remove(p.getUuid());
            if(oldT.getUuid().equals(t.getUuid())){
                ScoreboardManager.sendLobby(p);
                ScoreboardManager.sendLobby(t);
                return true;
            }
        }

        if(!t.getInvites()){
            p.sendMessage(Message.CHALLANGE_DEACTIVE);
            return true;
        }

        QueueManager.requests.put(p.getUuid(),t.getUuid());
        p.sendMessage(Message.CHALLANGE_CHALLANGE_START_SUCCESS.replace("player", t.getName()));
        t.playSound(t.thePlayer().getLocation(), Sound.ORB_PICKUP, 1, 1);
        p.playSound(t.thePlayer().getLocation(), Sound.ORB_PICKUP, 1, 1);
        TextComponent accept = new TextComponent("§8[§aAkzeptieren§8]");
        accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/c " + p.getName()));
        accept.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Akzeptiere die Herausforderung").create()));
        t.thePlayer().spigot().sendMessage(new TextComponent(Message.CHALLANGE_CHALLANGE_START_NOTIFY.replace("player", p.getName()).getContent()), accept);
        ScoreboardManager.sendLobby(p);
        ScoreboardManager.sendLobby(t);
        return true;
    }
}
