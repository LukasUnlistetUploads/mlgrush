package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DebugCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {


        RushPlayer p = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());

        if(args.length == 0){
            return false;
        }

        switch (args[0]){
            case "duel":
                if(p.isLobby()){
                    p.thePlayer().sendMessage(" Dein Dual ist null");
                }else {
                    p.thePlayer().sendMessage("Map: " + Duels.getMap(p.getUuid()));
                    p.thePlayer().sendMessage("Gegner;" + MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid())).getName());
                    p.thePlayer().sendMessage("Rot: " + MLGRush.getInstance().getPlayer(Duels.getRedPlayer(p.getUuid())).getName() + ":" + Duels.getRedScore(p.getUuid()));
                    p.thePlayer().sendMessage("Blau: " + MLGRush.getInstance().getPlayer(Duels.getBluePlayer(p.getUuid())).getName() + ":" + Duels.getBlueScore(p.getUuid()));
                }
        }


        return true;
    }
}
