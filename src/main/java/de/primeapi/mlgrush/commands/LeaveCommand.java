package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.managers.CoinsManager;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            return false;
        }

        RushPlayer p  = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());


        if(p.isLobby()){
            p.sendMessage(Message.QUIT_NOMATCH);
            return true;
        }

        RushPlayer opp = MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid()));
        opp.addWins(1);
        opp.addRounds(1);
        p.addLosses(1);
        p.addRounds(1);
        opp.sendMessage(Message.DUELL_WINNER.replace("player", p.getName()));
        p.sendMessage(Message.DUELL_LOOSER.replace("player", opp.getName()));
        opp.playSound(opp.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
        p.playSound(p.thePlayer().getLocation(), Sound.ANVIL_BREAK, 1, 1);
        Duels.end(p.getUuid());
        CoinsManager.addCoins(opp.getUuid(), CoinsManager.CoinsType.WIN);



        return true;
    }
}
