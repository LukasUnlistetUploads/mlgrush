package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.utilities.Names;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SetQueueCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            return false;
        }

        RushPlayer p  = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());


        if(!p.hasPermission("mlgrush.setqueue")) return false;

        Witch v = (Witch)p.thePlayer().getLocation().getWorld().spawnEntity(p.thePlayer().getLocation(), EntityType.WITCH);
        v.setCustomName(Names.WITCH_NAME);
        v.setCustomNameVisible(false);
        v.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 365000, 365000));
        Entity nmsEnt = ((CraftEntity)v).getHandle();
        NBTTagCompound tag = nmsEnt.getNBTTag();
        if (tag == null)
            tag = new NBTTagCompound();
        nmsEnt.c(tag);
        tag.setInt("NoAI", 1);
        tag.setInt("Silent", 1);
        nmsEnt.f(tag);

        return false;
    }
}
