package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.config.MapsConfig;
import de.primeapi.mlgrush.config.SettingsConfig;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class SetStatsWallCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)){
            return false;
        }

        RushPlayer p  = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());

        if(!p.hasPermission("mlgrush.setup")){
            p.sendNoPerm();
            return false;
        }

        // /setstatswall <int> <sign/head>

        if(args.length != 2){
            p.sendMessage(Message.SETSTATSWALL_USAGE);
            return true;
        }

        int i;
        try {
            i = Integer.parseInt(args[0]);
        }catch (Exception ex){
            p.sendMessage(Message.SETSTATSWALL_NONUMBER);
            return true;
        }
        Set<Material> set = null;
        Block block = p.thePlayer().getTargetBlock(set, Integer.MAX_VALUE);

        if(block == null){
            p.sendMessage(Message.SETSTATSWALL_NOBLOCK);
            return true;
        }

        if(args[1].equalsIgnoreCase("SIGN")){
            if(!(block.getState() instanceof Sign)) {
                p.sendMessage(Message.SETSTATSWALL_NOSIGN);
                return true;
            }
            Sign sign = (Sign) block.getState();
            sign.setLine(0,"------");
            sign.setLine(1,"Schild wurde");
            sign.setLine(2,"Erstellt");
            sign.setLine(3,"------");
            sign.update();
            MapsConfig.saveLocation(block.getLocation(), "statswall." + i + ".sign");
            if(i > SettingsConfig.cfg.getInt("settings.statswall.amount")){
                SettingsConfig.cfg.set("settings.statswall.amount", i);
                try {
                    SettingsConfig.save(SettingsConfig.cfg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else if(args[1].equalsIgnoreCase("HEAD")){
            if(!(block.getState() instanceof Skull)) {
                p.sendMessage(Message.SETSTATSWALL_NOHEAD);
                return true;
            }
            Skull skull = (Skull) block.getState();
            skull.setSkullType(SkullType.PLAYER);
            skull.setOwner("MHF_Question");
            skull.update();
            MapsConfig.saveLocation(block.getLocation(), "statswall." + i + ".head");
            if(i > SettingsConfig.cfg.getInt("settings.statswall.amount")){
                SettingsConfig.cfg.set("settings.statswall.amount", i);
                try {
                    SettingsConfig.save(SettingsConfig.cfg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else {
            p.sendMessage(Message.SETSTATSWALL_USAGE);
        }


        return true;
    }
}
