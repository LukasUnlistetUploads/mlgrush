package de.primeapi.mlgrush.commands;

import com.google.gson.internal.$Gson$Preconditions;
import com.sun.org.apache.bcel.internal.generic.FADD;
import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.api.Setup;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetupCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        RushPlayer p = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());

        if(!p.hasPermission("mlgrush.setup")){
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0) {
            p.sendMessage(Message.SETUP_USAGE_1);
            p.sendMessage(Message.SETUP_USAGE_2);
            return true;
        }

        if(args[0].equalsIgnoreCase("confirm")){
            MLGRush.getInstance().setup = new Setup(p.thePlayer());
            return true;
        }
        return false;
    }
}
