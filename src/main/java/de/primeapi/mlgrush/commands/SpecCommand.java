package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.managers.InventoryManager;
import de.primeapi.mlgrush.managers.ScoreboardManager;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class SpecCommand implements CommandExecutor {

    public static HashMap<UUID,UUID> curr = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        RushPlayer p = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());
        if(!p.hasPermission("mlgrush.spec")){
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0){
            if(curr.containsKey(p.getUuid())){
                curr.remove(p.getUuid());
                p.sendMessage(Message.SPEC_LEAVE);
                stopSpec(p);
                return true;
            }
            p.sendMessage(Message.SPEC_USAGE);
            return true;
        }

        Player t = Bukkit.getPlayer(args[0]);
        if(t == null){
            p.sendMessage(Message.SPEC_NOTFOUND);
            return true;
        }
        RushPlayer target = MLGRush.getInstance().getPlayer(t.getUniqueId());

        if(curr.containsKey(p.getUuid())){
            curr.remove(p.getUuid());
            stopSpec(p);
        }

        if(target.isLobby()){
            p.sendMessage(Message.SPEC_NOMATCH);
            return true;
        }

        p.sendMessage(Message.SPEC_SUCCESS.replace("player", t.getName()));
        curr.put(p.getUuid(), t.getUniqueId());
        p.thePlayer().teleport(t);
        p.thePlayer().showPlayer(t);
        p.thePlayer().showPlayer(MLGRush.getInstance().getPlayer(Duels.getOpponent(t.getUniqueId())).thePlayer());
        p.thePlayer().getInventory().clear();
        p.thePlayer().setGameMode(GameMode.ADVENTURE);
        p.thePlayer().setAllowFlight(true);
        return true;
    }

    public static void stopSpec(RushPlayer p){
        p.teleportSpawn();
        ScoreboardManager.sendLobby(p);
        InventoryManager.setHotbar(p);
        p.thePlayer().setGameMode(GameMode.SURVIVAL);
        p.thePlayer().setAllowFlight(false);

    }
}
