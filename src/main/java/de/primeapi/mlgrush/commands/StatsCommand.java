package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.managers.messages.Message;
import de.primeapi.mlgrush.sql.MySQL;
import de.primeapi.mlgrush.sql.SQLPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

public class StatsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }

        RushPlayer p = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());


        if(args.length > 0) {
            if(args[0].equalsIgnoreCase("reset")){
                if(!p.hasPermission("mlgrush.stats.reset")){
                    p.sendNoPerm();
                    return true;
                }
                if(args.length != 2){
                    p.sendMessage(Message.STATS_RESET_USAGE);
                    return true;
                }
                SQLPlayer t;
                try {
                    t = new SQLPlayer(args[1]);
                }catch (Exception ex){
                    p.sendMessage(Message.STATS_RESET_NOTFOUND);
                    return true;
                }

                t.setBeds(0);
                t.setKills(0);
                t.setDeaths(0);
                t.setWins(0);
                t.setLooses(0);
                t.setRounds(0);
                p.sendMessage(Message.STATS_RESET_SUCCESS.replace("player", t.getRealname()));
                return true;
            }
        }

        SQLPlayer t;
        if(args.length == 0){
            t = p;
        }else {
            try {
                t = new SQLPlayer(args[0]);
            }catch (IllegalArgumentException exception){
                p.sendMessage(Message.STATS_NOTFOUND);
                return true;
            }
        }




        p.sendMessage(Message.STATS_1);

        p.sendMessage(Message.STATS_2.replace("count", t.getRank()));
        p.sendMessage(Message.STATS_3.replace("count", t.getKills()));
        p.sendMessage(Message.STATS_4.replace("count", t.getDeaths()));
        p.sendMessage(Message.STATS_5.replace("count", t.getKD()));
        p.sendMessage(Message.STATS_6.replace("count", t.getWins()));
        p.sendMessage(Message.STATS_7.replace("count", t.getLooses()));
        p.sendMessage(Message.STATS_8.replace("count", t.getWL()));
        p.sendMessage(Message.STATS_9.replace("count", t.getBeds()));

        p.sendMessage(Message.STATS_10);


        return true;
    }
}
