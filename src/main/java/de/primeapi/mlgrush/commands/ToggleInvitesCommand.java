package de.primeapi.mlgrush.commands;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.List;

public class ToggleInvitesCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            return false;
        }

        RushPlayer p = MLGRush.getInstance().getPlayer(((Player) commandSender).getUniqueId());

        if(p.getInvites()){
            p.sendMessage(Message.TOGGLEINVITES_OFF);
            p.playSound(Sound.ORB_PICKUP, 1, 1);
            p.setInvites(false);
        }else {
            p.sendMessage(Message.TOGGLEINVITES_ON);
            p.playSound(Sound.ORB_PICKUP, 1, 1);
            p.setInvites(true);
        }


        return true;
    }
}
