package de.primeapi.mlgrush.commands.tabcompletion;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.List;

public class SpecCompleter implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        List<String> list = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(player -> {
            if(Duels.bluePlayer.containsKey(player.getUniqueId())) list.add(player.getName());
        });

        if(list.size() == 0) commandSender.sendMessage(Message.TABCOMPLETE_NOPLAYERS.getContent());
        return list;
    }
}
