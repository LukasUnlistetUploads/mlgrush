package de.primeapi.mlgrush.config;

import de.primeapi.mlgrush.MLGRush;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;

public class
MapsConfig {

    public static File file;
    public static YamlConfiguration cfg;

    public static void init() throws IOException {
        File ord = new File("plugins/mlgrush");
        if(!ord.exists()) ord.mkdir();

        file = new File("plugins/mlgrush", "locations.yml");
        if(!file.exists()) file.createNewFile();

        reload();
        fill();
    }

    public static void reload(){
        cfg = YamlConfiguration.loadConfiguration(file);
    }

    public static void save() throws IOException {
        cfg.save(file);
    }

    public static void save(YamlConfiguration cfg) throws IOException {
        cfg.save(file);
    }

    public static void fill(){
        MLGRush.getInstance().getThreadPoolExecutor().submit(() -> {
            if(!cfg.contains("maps.amount")){
                cfg.set("maps.amount", 0);
                try {
                    save(cfg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void saveLocation(Location loc,String name){
        cfg.set("locations." + name + ".x", loc.getX());
        cfg.set("locations." + name + ".y", loc.getY());
        cfg.set("locations." + name + ".z", loc.getZ());
        cfg.set("locations." + name + ".yaw", loc.getYaw());
        cfg.set("locations." + name + ".pitch", loc.getPitch());
        cfg.set("locations." + name + ".world", loc.getWorld().getName());
        try {
            save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Location getLocation(String name){
        double x = cfg.getDouble("locations." + name + ".x");
        double y = cfg.getDouble("locations." + name + ".y");
        double z = cfg.getDouble("locations." + name + ".z");
        float yaw = cfg.getInt("locations." + name + ".yaw");
        float pitch = cfg.getInt("locations." + name + ".pitch");
        World world = Bukkit.getWorld(cfg.getString("locations." + name + ".world"));
        return new Location(world,x,y,z,yaw,pitch);
    }

    public static Integer getMapsAmount(){
        return cfg.getInt("maps.amount");
    }

    public static void addMap(){
        cfg.set("maps.amount", getMapsAmount() + 1);
        try {
            save();
        } catch (IOException e) {
            e.printStackTrace();
        }
        reload();
    }


}
