package de.primeapi.mlgrush.config;

import de.primeapi.mlgrush.MLGRush;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SettingsConfig {

    public static File file;
    public static YamlConfiguration cfg;

    public static void init() throws IOException {
        File ord = new File("plugins/mlgrush");
        if(!ord.exists()) ord.mkdir();

        file = new File("plugins/mlgrush", "settings.yml");
        if(!file.exists()) file.createNewFile();

        reload();
        fill();
        reload();
    }

    public static void reload(){
        cfg = YamlConfiguration.loadConfiguration(file);
    }

    public static void save() throws IOException {
        cfg.save(file);
    }

    public static void save(YamlConfiguration cfg) throws IOException {
        cfg.save(file);
    }

    public static void fill(){
        MLGRush.getInstance().getThreadPoolExecutor().submit(() -> {
           if(!cfg.contains("mysql.host")){
               cfg.options().header("Author: PrimeAPI, Discord: discord.primeapi.de, Plugin-Version:" + MLGRush.getInstance().getDescription().getVersion());
               cfg.set("settings.license", "request one on the Discord");
               cfg.set("settings.prefix", "§7➤ §6Prefix §7✦");
               cfg.set("settings.pointstowin", 15);
               cfg.set("settings.levelOfKnockBack", 2);
               cfg.set("settings.statswall.use", true);
               cfg.set("settings.statswall.amount", 0);
               cfg.set("settings.levelPoints", true);
               cfg.set("settings.effects.sound", true);
               cfg.set("settings.effects.title.use", true);
               cfg.set("settings.effects.title.line.1", "§7x §6Dein-Server.net §7x");
               cfg.set("settings.effects.title.line.2", "§8- §6MLGRush §8-");

               cfg.set("mysql.host", "localhost:3306");
               cfg.set("mysql.database", "mlgrush");
               cfg.set("mysql.username", "root");
               cfg.set("mysql.password", "password");


               List<String> ingame = new ArrayList<>();
               ingame.add("§1");
               ingame.add("§7Gegner:");
               ingame.add("§8➥ §e%opponent%");
               ingame.add("§2");
               ingame.add("§7Punkte (Du):");
               ingame.add("§8➥ §e%points_own%§1");
               ingame.add("§3");
               ingame.add("§7Punkte (Gegner):");
               ingame.add("§8➥ §e%point_opponent%§2");
               ingame.add("§4");
               cfg.set("settings.scoreboard.ingame.content", ingame);
               cfg.set("settings.scoreboard.ingame.title", "§7» §eMLG-Rush §7«");


               List<String> lobby = new ArrayList<>();
               lobby.add("§1");
               lobby.add("§7Platzierung:");
               lobby.add("§8➥ §e%rank%");
               lobby.add("§2");
               lobby.add("§7Herausvorderung [>>]");
               lobby.add("§8➥ §e%sent%§1");
               lobby.add("§3");
               lobby.add("§7Herausvorderung [<<]");
               lobby.add("%REQUESTS%");
               lobby.add("§4");
               cfg.set("settings.scoreboard.lobby.content", lobby);
               cfg.set("settings.scoreboard.lobby.request", "§8➥ §e%name%");
               cfg.set("settings.scoreboard.lobby.title", "§7» §eMLG-Rush §7«");


               cfg.set("coins.amount.kill", 5);
               cfg.set("coins.amount.bed", 10);
               cfg.set("coins.amount.win", 50);
               cfg.set("coins.message", "&7➤ &6Prefix &7✦ &7Du hast &e%amount% &7Coins &aerhalten&7!");
               try {
                   save(cfg);
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
        });
    }

}
