package de.primeapi.mlgrush.lcn;


import de.primeapi.mlgrush.MLGRush;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;

public class LCN {

    private String PluginID = "MLGRush";

    private String licenseKey;
    private String validationServer = "http://primeapi.de/shop/licence/verify.php";
    private LogType logType = LogType.NORMAL;
    private String securityKey = "YecoF0I6M05thxLeokoHuW8iUhTdIUInjkfF";
    private boolean debug = false;
    private ValidationType cache;

    public LCN(String licenseKey, String validationServer) {
        this.licenseKey = licenseKey;
        this.validationServer = validationServer;
    }

    private static String xor(String s1, String s2) {
        String s0 = "";
        for (int i = 0; i < (s1.length() < s2.length() ? s1.length() : s2.length()); i++)
            s0 += Byte.valueOf("" + s1.charAt(i)) ^ Byte.valueOf("" + s2.charAt(i));
        return s0;
    }

    public LCN setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
        return this;
    }

    public LCN setConsoleLog(LogType logType) {
        this.logType = logType;
        return this;
    }

    public LCN debug() {
        debug = true;
        return this;
    }

    public boolean register() {
        ValidationType vt = isValid();
        if (vt.equals(ValidationType.INVALID_PLUGIN)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("LizenzKey ist NICHT für das MLGRush!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Type: " + vt.toString());
            System.out.println("Stoppe Server!");
            System.out.println("[]==========[License-System]==========[]");
            return false;

        } else if (vt.equals(ValidationType.KEY_NOT_FOUND)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("LizenzKey ist NICHT gültig!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Type: " + vt.toString());
            System.out.println("Stoppe Server!");
            System.out.println("[]==========[License-System]==========[]");
            return false;

        } else if (vt.equals(ValidationType.KEY_OUTDATED)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("LizenzKey ist ausgelaufen!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Type: " + vt.toString());
            System.out.println("Stoppe Server!");
            System.out.println("[]==========[License-System]==========[]");
            return false;

        } else if (vt.equals(ValidationType.NOT_VALID_IP)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("Maximale anzahl an IP's erreicht!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Type: " + vt.toString());
            System.out.println("Stoppe Server!");
            System.out.println("[]==========[License-System]==========[]");
            return false;

        } else if (vt.equals(ValidationType.PAGE_ERROR)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("Fehler bei der Abfrage!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Type: " + vt.toString());
            System.out.println("Stoppe Server!");
            System.out.println("[]==========[License-System]==========[]");
            return false;

        } else if (vt.equals(ValidationType.URL_ERROR)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("Fehler bei der Abfrage!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Type: " + vt.toString());
            System.out.println("Stoppe Server!");
            System.out.println("[]==========[License-System]==========[]");
            return false;

        } else if (vt.equals(ValidationType.WRONG_RESPONSE)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("Fehler bei der Abfrage!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Type: " + vt.toString());
            System.out.println("Stoppe Server!");
            System.out.println("[]==========[License-System]==========[]");
            return false;

        } else if (vt.equals(ValidationType.VALID)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("LizenzKey ist gültig!");
            System.out.println("[]==========[License-System]==========[]");
            return true;
        } else {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("Es ist ein Fehler aufgetreten!");
            System.out.println("[]==========[License-System]==========[]");
            return false;
        }
    }


    //
    // Cryptographic
    //

    public boolean isValidSimple() {
        return (isValid() == ValidationType.VALID);
    }
    


    public ValidationType isValid() {

        if (Objects.nonNull(this.cache)) return this.cache;

        String rand = toBinary(UUID.randomUUID().toString());
        String sKey = toBinary(securityKey);
        String key = toBinary(licenseKey);
        String version = String.valueOf(MLGRush.getInstance().getDescription().getVersion());


        try {
            URL url = new URL(validationServer + "?v1=" + xor(rand, sKey) + "&v2=" + xor(rand, key) + "&pl=" + PluginID + "&version=" + version);
            if (debug) System.out.println("RequestURL -> " + url.toString());
            Scanner s = new Scanner(url.openStream());
            if (s.hasNext()) {
                String response = s.next();
                s.close();
                try {
                    return ValidationType.valueOf(response);
                } catch (IllegalArgumentException exc) {
                    String respRand = xor(xor(response, key), sKey);
                    if (rand.substring(0, respRand.length()).equals(respRand)) this.cache = ValidationType.VALID;
                    else this.cache = ValidationType.WRONG_RESPONSE;
                }
            } else {
                s.close();
                return ValidationType.PAGE_ERROR;
            }
        } catch (IOException exc) {
            if (debug) exc.printStackTrace();
            this.cache = ValidationType.URL_ERROR;
        }
        return this.cache;
    }

    //
    // Enums
    //

    private String toBinary(String s) {
        byte[] bytes = s.getBytes();
        StringBuilder binary = new StringBuilder();
        for (byte b : bytes) {
            int val = b;
            for (int i = 0; i < 8; i++) {
                binary.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }
        return binary.toString();
    }

    private void log(int type, String message) {
        if (logType == LogType.NONE || (logType == LogType.LOW && type == 0)) return;
        System.out.println(message);
    }

    //
    // Binary methods
    //

    public enum LogType {
        NORMAL, LOW, NONE
    }

    //
    // Console-Log
    //
    public enum ValidationType {

        KEY_NOT_FOUND(""),
        WRONG_RESPONSE("fa"),
        PAGE_ERROR("fa"),
        URL_ERROR("fa"),
        KEY_OUTDATED("fa"),
        NOT_VALID_IP("fa"),
        INVALID_PLUGIN("fa"),
        VALID("fa");

        final String errorMessage;

        ValidationType(String s) {
            this.errorMessage = s;
        }

    }
}
