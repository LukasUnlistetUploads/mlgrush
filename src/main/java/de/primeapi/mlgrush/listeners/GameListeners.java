package de.primeapi.mlgrush.listeners;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.config.MapsConfig;
import de.primeapi.mlgrush.config.SettingsConfig;
import de.primeapi.mlgrush.managers.CoinsManager;
import de.primeapi.mlgrush.managers.InventoryManager;
import de.primeapi.mlgrush.managers.ScoreboardManager;
import de.primeapi.mlgrush.managers.messages.Message;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Objects;

public class GameListeners implements Listener {



    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if(!(e.getEntity() instanceof Player)){
            return;
        }
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getEntity().getUniqueId());
        if(!p.isLobby()){
            if(Duels.getWait(p.getUuid())){
                e.setCancelled(true);
            }
            if(e.getCause() == EntityDamageEvent.DamageCause.FALL){
                e.setCancelled(true);
                return;
            }
            e.setCancelled(false);
            e.setDamage(0.0D);
        }
    }

    @EventHandler
    public void onDamagee(EntityDamageEvent e){
        if(Duels.wait.containsKey(e.getEntity().getUniqueId())) {
            if (Duels.getWait(e.getEntity().getUniqueId())) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDamagee(EntityDamageByEntityEvent e){
        if(!(e.getEntity() instanceof Player)){
            return;
        }
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getEntity().getUniqueId());
        if(p.isLobby()){
            e.setCancelled(true);
        }
    }


    int respawnUnderSpawn = 20;
    @EventHandler
    public void onMove(PlayerMoveEvent e){
        if(Duels.bluePlayer.containsKey(e.getPlayer().getUniqueId())) {


            Location loc = e.getPlayer().getLocation();
            int map = Duels.getMap(e.getPlayer().getUniqueId());
            Location arena1 = MapsConfig.getLocation("maps." + map + ".region.1");
            Location arena2 = MapsConfig.getLocation("maps." + map + ".region.2");

            if(loc.getX() < arena1.getX() || loc.getY() < arena1.getY() || loc.getZ() < arena1.getZ()){
                RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
                RushPlayer opp = MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid()));
                p.addDeaths(1);
                opp.addKills(1);
                CoinsManager.addCoins(opp.getUuid(), CoinsManager.CoinsType.KILL);
                if(Duels.getRedPlayer(p.getUuid()).equals(p.getUuid())){
                    p.thePlayer().teleport(MapsConfig.getLocation("maps." + Duels.getMap(p.getUuid()) + ".red.spawn"));
                }else {
                    p.thePlayer().teleport(MapsConfig.getLocation("maps." + Duels.getMap(p.getUuid()) + ".blue.spawn"));
                }
                InventoryManager.setIngame(p);
                opp.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
            }

            if(loc.getX() > arena2.getX() || loc.getZ() > arena2.getZ()){
                RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
                RushPlayer opp = MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid()));
                p.addDeaths(1);
                opp.addKills(1);
                CoinsManager.addCoins(opp.getUuid(), CoinsManager.CoinsType.KILL);
                if(Duels.getRedPlayer(p.getUuid()).equals(p.getUuid())){
                    p.thePlayer().teleport(MapsConfig.getLocation("maps." + Duels.getMap(p.getUuid()) + ".red.spawn"));
                    p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                }else {
                    p.thePlayer().teleport(MapsConfig.getLocation("maps." + Duels.getMap(p.getUuid()) + ".blue.spawn"));
                    p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                }
                InventoryManager.setIngame(p);
                p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
            }



            if (e.getPlayer().getLocation().getY() < MapsConfig.getLocation("maps." + Duels.getMap(e.getPlayer().getUniqueId()) + ".red.spawn").getBlockY() - respawnUnderSpawn){
                RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
                RushPlayer opp = MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid()));
                p.addDeaths(1);
                opp.addKills(1);
                CoinsManager.addCoins(opp.getUuid(), CoinsManager.CoinsType.KILL);
                if(Duels.getRedPlayer(p.getUuid()).equals(p.getUuid())){
                    p.thePlayer().teleport(MapsConfig.getLocation("maps." + Duels.getMap(p.getUuid()) + ".red.spawn"));
                    p.thePlayer().sendTitle("§7× §6MLGRush §7×", "§8- §cViel Glück! §8-");
                    p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                }else {
                    p.thePlayer().teleport(MapsConfig.getLocation("maps." + Duels.getMap(p.getUuid()) + ".blue.spawn"));
                    p.thePlayer().sendTitle("§7× §6MLGRush §7×", "§8- §cViel Glück! §8-");
                    p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                }
                InventoryManager.setIngame(p);
            }
        }
    }

    @EventHandler
    public void onBedEnter(PlayerBedEnterEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e){
        Player player = e.getPlayer();
        if(Duels.bluePlayer.containsKey(player.getUniqueId())) {
            int map = Duels.getMap(player.getUniqueId());
            if(e.getBlock().getType() == Material.BED_BLOCK) {

                RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
                RushPlayer opp = MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid()));
                if (e.getBlock().getLocation().equals(MapsConfig.getLocation("maps." + map + ".red.bed.1")) || e.getBlock().getLocation().equals(MapsConfig.getLocation("maps." + map + ".red.bed.2"))) {
                    e.setCancelled(true);
                    if (Duels.getRedPlayer(player.getUniqueId()).equals(player.getUniqueId())) {
                        p.sendMessage(Message.BEDS_OWN);
                        return;
                    }
                    Duels.addScoreBlue(p.getUuid(), 1);
                    if(SettingsConfig.cfg.getBoolean("settings.levelPoints")) p.thePlayer().setLevel(Duels.getBlueScore(p.getUuid()));
                    p.sendMessage(Message.BEDS_DESTROY_RED.replace("player", p.getName()));
                    p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                    opp.sendMessage(Message.BEDS_DESTROY_RED.replace("player", p.getName()));
                    opp.playSound(p.thePlayer().getLocation(), Sound.ANVIL_LAND, 1, 1);
                    p.addBeds(1);
                    CoinsManager.addCoins(p.getUuid(), CoinsManager.CoinsType.BED);
                }
                if (e.getBlock().getLocation().equals(MapsConfig.getLocation("maps." + map + ".blue.bed.1")) || e.getBlock().getLocation().equals(MapsConfig.getLocation("maps." + map + ".blue.bed.2"))) {
                    e.setCancelled(true);
                    if (Duels.getBluePlayer(p.getUuid()).equals(p.getUuid())) {
                        p.sendMessage(Message.BEDS_OWN);
                        return;
                    }
                    Duels.addScoreRed(p.getUuid(), 1);
                    if(SettingsConfig.cfg.getBoolean("settings.levelPoints")) p.thePlayer().setLevel(Duels.getRedScore(p.getUuid()));
                    p.sendMessage(Message.BEDS_DETROY_BLUE.replace("player", p.getName()));
                    p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                    opp.sendMessage(Message.BEDS_DETROY_BLUE.replace("player", p.getName()));
                    opp.playSound(p.thePlayer().getLocation(), Sound.ANVIL_LAND, 1, 1);
                    p.addBeds(1);
                    CoinsManager.addCoins(p.getUuid(), CoinsManager.CoinsType.BED);
                }

                MLGRush.getInstance().getPlayer(Duels.getBluePlayer(p.getUuid())).thePlayer().teleport(MapsConfig.getLocation("maps." + map + ".blue.spawn"));
                MLGRush.getInstance().getPlayer(Duels.getRedPlayer(p.getUuid())).thePlayer().teleport(MapsConfig.getLocation("maps." + map + ".red.spawn"));
                ScoreboardManager.sendIngame(p);
                ScoreboardManager.sendIngame(opp);
                InventoryManager.setIngame(p);
                InventoryManager.setIngame(opp);
                for(Location loc : Duels.getPlacedBlocks(p.getUuid())){
                    loc.getBlock().setType(Material.AIR);
                }
                Duels.clearPlacedBlock(p.getUuid());

                if (Duels.getRedScore(p.getUuid()) >= SettingsConfig.cfg.getInt("settings.pointstowin")) {
                    RushPlayer winner = MLGRush.getInstance().getPlayer(Duels.getRedPlayer(p.getUuid()));
                    RushPlayer looser = MLGRush.getInstance().getPlayer(Duels.getBluePlayer(p.getUuid()));
                    winner.addWins(1);
                    winner.addRounds(1);
                    looser.addLosses(1);
                    looser.addRounds(1);
                    winner.sendMessage(Message.DUELL_WINNER.replace("player", looser.getName()));
                    looser.sendMessage(Message.DUELL_LOOSER.replace("player", winner.getName()));
                    winner.playSound(opp.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                    looser.playSound(p.thePlayer().getLocation(), Sound.ANVIL_BREAK, 1, 1);
                    Duels.end(p.getUuid());
                    CoinsManager.addCoins(winner.getUuid(), CoinsManager.CoinsType.WIN);
                }
                if (Duels.getBlueScore(p.getUuid()) >= SettingsConfig.cfg.getInt("settings.pointstowin")) {
                    RushPlayer winner = MLGRush.getInstance().getPlayer(Duels.getBluePlayer(p.getUuid()));
                    RushPlayer looser = MLGRush.getInstance().getPlayer(Duels.getRedPlayer(p.getUuid()));
                    winner.addWins(1);
                    winner.addRounds(1);
                    looser.addLosses(1);
                    looser.addRounds(1);
                    winner.sendMessage(Message.DUELL_WINNER.replace("player", looser.getName()));
                    looser.sendMessage(Message.DUELL_LOOSER.replace("player", winner.getName()));
                    winner.playSound(opp.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
                    looser.playSound(p.thePlayer().getLocation(), Sound.ANVIL_BREAK, 1, 1);
                    Duels.end(p.getUuid());
                    CoinsManager.addCoins(winner.getUuid(), CoinsManager.CoinsType.WIN);
                }
            }

            if(!Objects.isNull(Duels.getPlacedBlocks(player.getUniqueId())) && !Objects.isNull(e.getBlock().getLocation())) {

                if (!Duels.getPlacedBlocks(player.getUniqueId()).contains(e.getBlock().getLocation())) {
                    e.setCancelled(true);
                } else {
                    e.setCancelled(false);
                    Duels.removePlacedBlock(player.getUniqueId(), e.getBlock().getLocation());
                }
            }

        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if(!p.isLobby()) {

            if(e.getBlock().getLocation().getBlockY() > MapsConfig.getLocation("maps." + Duels.getMap(p.getUuid()) + ".region.2").getBlockY() - 1){
                e.setCancelled(true);
                return;
            }

            Duels.addPlacedBlock(p.getUuid(), e.getBlock().getLocation());
        }
    }

}
