package de.primeapi.mlgrush.listeners;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.config.MapsConfig;
import de.primeapi.mlgrush.config.SettingsConfig;
import de.primeapi.mlgrush.managers.*;
import de.primeapi.mlgrush.managers.messages.Message;
import de.primeapi.mlgrush.utilities.Names;
import de.primeapi.mlgrush.utilities.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

import java.util.UUID;

public class GeneralListeners implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        e.setJoinMessage(null);
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
        p.setName(p.thePlayer().getName().toLowerCase());
        p.setRealname(p.thePlayer().getName());
        p.thePlayer().setLevel(0);
        p.thePlayer().setExp(0);

        for(Player allPlayer : Bukkit.getOnlinePlayers()){
            RushPlayer all = MLGRush.getInstance().getPlayer(allPlayer.getUniqueId());
            if(!all.isLobby() || DuelManagers.waiting.containsKey(all.getUuid())){
                all.thePlayer().hidePlayer(p.thePlayer());
                p.thePlayer().hidePlayer(all.thePlayer());
            }else {
                all.thePlayer().showPlayer(p.thePlayer());
                p.thePlayer().showPlayer(all.thePlayer());
            }
        }
        if(SettingsConfig.cfg.getBoolean("settings.effects.title.use")) {
            p.thePlayer().sendTitle(SettingsConfig.cfg.getString("settings.effects.title.line.1"),SettingsConfig.cfg.getString("settings.effects.title.line.2"));
        }
        p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
        p.thePlayer().setGameMode(GameMode.SURVIVAL);

        InventoryManager.setHotbar(p);
        System.out.println("GeneralListeners.onJoin");
        ScoreboardManager.sendLobby(p);
        System.out.println("GeneralListeners.onJoin");
    }


    @EventHandler
    public void onSpawnLocation(PlayerSpawnLocationEvent e){
        try {
            e.setSpawnLocation(MapsConfig.getLocation("spawn"));
        }catch (Exception ex){
            e.getPlayer().sendMessage(MLGRush.getInstance().getPrefix() + "§cDer Spawn wurde noch nicht gesetzt! §6/setup");
        }
    }

    @EventHandler
    public void onHunger(FoodLevelChangeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemMove(InventoryMoveItemEvent e){
        if(!e.getDestination().getTitle().equals(Names.SORT_TITLE)){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e){

        RushPlayer p = MLGRush.getInstance().getPlayer(e.getWhoClicked().getUniqueId());
        if (p.thePlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if(p.isLobby()){
            if(e.getClickedInventory() == null) return;
            if(e.getClickedInventory().getTitle().equals(Names.SORT_TITLE)){
                e.setCancelled(false);
            }else {
                e.setCancelled(true);
            }
        }else {
            e.setCancelled(false);
        }
    }

    @EventHandler
    public void onSleep(PlayerBedEnterEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onPing(ServerListPingEvent e){
        e.setMaxPlayers(32);
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        e.setQuitMessage(null);
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if(Duels.redPlayer.containsKey(p.getUuid())){
            RushPlayer opp = MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid()));
            opp.addWins(1);
            opp.addRounds(1);
            p.addLosses(1);
            p.addRounds(1);
            opp.sendMessage(Message.DUELL_WINNER.replace("player", p.getName()));
            p.sendMessage(Message.DUELL_LOOSER.replace("player", opp.getName()));
            opp.playSound(opp.thePlayer().getLocation(), Sound.LEVEL_UP, 100, 100);
            p.playSound(p.thePlayer().getLocation(), Sound.ANVIL_BREAK, 1, 1);
            Duels.end(p.getUuid());
            CoinsManager.addCoins(opp.getUuid(), CoinsManager.CoinsType.WIN);
        }

        if(DuelManagers.waiting.containsKey(p.getUuid())){
            RushPlayer opp = MLGRush.getInstance().getPlayer(DuelManagers.waiting.get(p.getUuid()));
            opp.sendMessage(Message.DUELL_LEAVE_NOTIFY.replace("player", p.getName()));
            Duels.end(p.getUuid());
        }


        if(QueueManager.requests.containsKey(p.getUuid())){
            RushPlayer t = MLGRush.getInstance().getPlayer(QueueManager.requests.get(p.getUuid()));
            p.sendMessage(Message.CHALLANGE_CHALLANGE_BACK_NOTIFY.replace("player", p.getName()));
            t.playSound(t.thePlayer().getLocation(), Sound.NOTE_BASS, 1, 1);
        }
        QueueManager.requests.remove(p.getUuid());
        QueueManager.queue.remove(p.getUuid());
        for(UUID uuid : QueueManager.requests.keySet()){
            if(QueueManager.requests.get(uuid).equals(p.getUuid())){
                RushPlayer t = MLGRush.getInstance().getPlayer(uuid);
                t.sendMessage(Message.DUELL_LEAVE_NOTIFY.replace("player", p.getName()));
                t.playSound(p.thePlayer().getLocation(), Sound.NOTE_BASS, 1, 1);
                QueueManager.requests.remove(uuid);
                ScoreboardManager.sendLobby(t);
            }
        }
        MLGRush.getInstance().unregisterPlayer(e.getPlayer().getUniqueId());
    }

}
