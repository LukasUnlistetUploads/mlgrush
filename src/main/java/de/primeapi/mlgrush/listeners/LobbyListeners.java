package de.primeapi.mlgrush.listeners;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.managers.InventoryManager;
import de.primeapi.mlgrush.managers.QueueManager;
import de.primeapi.mlgrush.managers.ScoreboardManager;
import de.primeapi.mlgrush.managers.messages.Message;
import de.primeapi.mlgrush.utilities.ItemBuilder;
import de.primeapi.mlgrush.utilities.Names;
import de.primeapi.mlgrush.utilities.guiapi.GUIBuilder;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Witch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scoreboard.Score;

import java.util.Objects;

public class LobbyListeners implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if(!(e.getEntity() instanceof Player)){
            return;
        }
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getEntity().getUniqueId());
        if(p.isLobby()){
            e.setCancelled(true);
        }   
    }

    @EventHandler
    public void onMave(PlayerMoveEvent e){
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if(p.isLobby() && e.getTo().getBlockY() <= 0){
            p.teleportSpawn();
            p.playSound(p.thePlayer().getLocation(), Sound.LEVEL_UP, 100 ,100);
            p.playSound(p.thePlayer().getLocation(), Sound.ENDERMAN_TELEPORT, 1 ,1);
        }
    }

    @EventHandler
    public void onPlayerInteractAtEntity(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player) {
            RushPlayer p = MLGRush.getInstance().getPlayer(e.getDamager().getUniqueId());
            if(p.isLobby()){
                e.setCancelled(true);
            }
            if (e.getEntity() instanceof Player) {
                if(p.thePlayer().getItemInHand() == null) return;
                if(p.thePlayer().getItemInHand().getItemMeta() == null) return;
                if(p.thePlayer().getItemInHand().getItemMeta().getDisplayName() == null) return;
                if (p.thePlayer().getItemInHand().getItemMeta().getDisplayName().equals(Names.HOTBAR_SWORD)) {
                    p.thePlayer().chat("/c " + e.getEntity().getName());
                    e.setCancelled(true);
                }
            }
            if (p.isLobby() && !p.thePlayer().getGameMode().equals(GameMode.CREATIVE)) {
                e.setCancelled(true);



                if(e.getEntity() instanceof Witch){
                    if(p.thePlayer().getItemInHand() == null) return;
                    if(p.thePlayer().getItemInHand().getItemMeta() == null) return;
                    if(p.thePlayer().getItemInHand().getItemMeta().getDisplayName() == null) return;
                    if (p.thePlayer().getItemInHand().getItemMeta().getDisplayName().equals(Names.HOTBAR_SWORD)) {
                        if (e.getEntity().getCustomName().equals(Names.WITCH_NAME)) {
                            p.thePlayer().chat("/c Queue");
                        }
                    }
                }

            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if(p.isLobby() && !p.thePlayer().getGameMode().equals(GameMode.CREATIVE)){
            e.setCancelled(true);
            if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                if(Objects.isNull(p.thePlayer().getItemInHand())) return;
                if(Objects.isNull(p.thePlayer().getItemInHand().getItemMeta())) return;
                if(Objects.isNull(p.thePlayer().getItemInHand().getItemMeta().getDisplayName())) return;
                String displayName = p.thePlayer().getItemInHand().getItemMeta().getDisplayName();
                if (Names.HOTBAR_SETTINGS.equals(displayName)) {
                    p.thePlayer().openInventory(
                    new GUIBuilder(3 * 9, Message.SETTINGS_TITLE.getContent()).fillInventory(new ItemBuilder(Material.STAINED_GLASS_PANE, (byte) 8).setDisplayName(" ").build())
                            .addItem(11, new ItemBuilder(Material.STONE_PICKAXE).setDisplayName(Message.SETTINGS_INVENTORY_SORT.getContent()).build(), (p1, itemStack) -> {
                                InventoryManager.openSort(p);
                            })
                            .addItem(15, new ItemBuilder(Material.DIAMOND_SWORD).setDisplayName(p.getInvites() ? Message.SETTINGS_ACCEPT_ON.getContent() : Message.SETTINGS_ACCEPT_OFF.getContent()).build(), (p1, itemStack) -> {
                                p.getPlayer().chat("/toggleinvites");
                                p.thePlayer().closeInventory();
                            }).build());

                    //InventoryManager.openSort(p);
                } else if (Names.HOTBAR_LEAVE.equals(displayName)) {
                    p.thePlayer().kickPlayer(MLGRush.getInstance().getPrefix() + "§7 Du wurdest auf die Lobby verschoben!");
                }
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if(p.isLobby() && !p.thePlayer().getGameMode().equals(GameMode.CREATIVE)){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e){
        RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if(p.isLobby() && !p.thePlayer().getGameMode().equals(GameMode.CREATIVE)){
            e.setCancelled(true);
        }
    }

}
