package de.primeapi.mlgrush.managers;


import de.primeapi.bungeesystem.spigot.mysql.CoinsAPI;
import de.primeapi.mlgrush.config.SettingsConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.Objects;
import java.util.UUID;

public class CoinsManager {


    public static void addCoins(UUID uuid, CoinsType type){
        int amount = 0;
        if (type == CoinsType.KILL) amount = SettingsConfig.cfg.getInt("coins.amount.kill");
        if (type == CoinsType.BED) amount = SettingsConfig.cfg.getInt("coins.amount.bed");
        if (type == CoinsType.WIN) amount = SettingsConfig.cfg.getInt("coins.amount.win");

        if(amount == 0) return;

        if(Objects.isNull((Bukkit.getPluginManager().getPlugin("BungeeSystemAdapter")))) return;

        CoinsAPI.addcoins(uuid.toString(), amount);
        try {
            Bukkit.getPlayer(uuid).sendMessage(ChatColor.translateAlternateColorCodes('&', SettingsConfig.cfg.getString("coins.message")).replaceAll("%amount%", String.valueOf(amount)));
        }catch (Exception ignored){}
    }


    public enum CoinsType{
        WIN,
        KILL,
        BED
    }

}
