package de.primeapi.mlgrush.managers;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.config.SettingsConfig;
import de.primeapi.mlgrush.managers.messages.Message;
import de.primeapi.mlgrush.utilities.InventoryBuilder;
import de.primeapi.mlgrush.utilities.ItemBuilder;
import de.primeapi.mlgrush.utilities.Names;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryManager implements Listener {

    public static void setHotbar(RushPlayer p){
        p.thePlayer().getInventory().clear();
        p.thePlayer().getInventory().setArmorContents(null);

        p.thePlayer().getInventory().setItem(0, new ItemBuilder(Material.DIAMOND_SWORD).setDisplayName(Names.HOTBAR_SWORD).build());
        p.thePlayer().getInventory().setItem(4, new ItemBuilder(Material.REDSTONE_COMPARATOR).setDisplayName(Names.HOTBAR_SETTINGS).build());
        p.thePlayer().getInventory().setItem(8, new ItemBuilder(Material.MAGMA_CREAM).setDisplayName(Names.HOTBAR_LEAVE).build());
    }

    public static void setIngame(RushPlayer p){
        p.thePlayer().getInventory().clear();
        p.thePlayer().getInventory().setArmorContents(null);

        p.thePlayer().getInventory().setItem(p.getStick(), new ItemBuilder(Material.STICK).addEnchantment(Enchantment.KNOCKBACK, SettingsConfig.cfg.getInt("settings.levelOfKnockBack")).build());
        p.thePlayer().getInventory().setItem(p.getPickaxe(), new ItemBuilder(Material.STONE_PICKAXE).build());
        p.thePlayer().getInventory().setItem(p.getBlocks(), new ItemBuilder(Material.SANDSTONE).setAmount(64).build());
    }

    public static void openSort(RushPlayer p){
        Inventory inv = new InventoryBuilder().setSize(9).setName(Names.SORT_TITLE).build();

        inv.setItem(p.getStick(), new ItemBuilder(Material.STICK).addEnchantment(Enchantment.KNOCKBACK, SettingsConfig.cfg.getInt("settings.levelOfKnockBack")).build());
        inv.setItem(p.getPickaxe(), new ItemBuilder(Material.STONE_PICKAXE).setDisplayName(Names.SORT_PICKAXE).build());
        inv.setItem(p.getBlocks(), new ItemBuilder(Material.SANDSTONE).setDisplayName(Names.SORT_BLOCKS).build());

        p.thePlayer().openInventory(inv);
    }

    @EventHandler
    public void onclode(InventoryCloseEvent e){
        if(e.getInventory().getTitle().equals(Names.SORT_TITLE)){
            RushPlayer p = MLGRush.getInstance().getPlayer(e.getPlayer().getUniqueId());
            int blocks = p.getBlocks();
            int stick = p.getStick();
            int pick = p.getPickaxe();

            for(int i = 0; i < 9; i++){
                ItemStack item = e.getInventory().getItem(i);
                if(item == null){
                    continue;
                }
                if(item.getItemMeta() == null) continue;
                if(item.getItemMeta().getDisplayName() == null) continue;
                if(item.getItemMeta().getDisplayName().equals(Names.SORT_BLOCKS)){
                    blocks = i;
                }
                if(item.getItemMeta().getDisplayName().equals(Names.SORT_STICK)){
                    stick = i;
                }
                if(item.getItemMeta().getDisplayName().equals(Names.SORT_PICKAXE)){
                    pick = i;
                }
            }
            if(blocks == stick || blocks == pick || stick == pick){
                stick = 0;
                pick = 1;
                blocks = 2;
                p.thePlayer().sendMessage("§c Es ist ein Fehler aufgetreten!");
                p.playSound(Sound.NOTE_BASS);
            }else {
                p.sendMessage(Message.INVSORT_SAVE);
                p.playSound(p.thePlayer().getLocation(), Sound.ORB_PICKUP, 1, 1);
            }
            p.setStick(stick);
            p.setPickaxe(pick);
            p.setBlocks(blocks);
            setHotbar(p);
        }
    }


}
