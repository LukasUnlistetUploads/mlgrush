package de.primeapi.mlgrush.managers;

import de.primeapi.mlgrush.config.MapsConfig;

import java.util.ArrayList;
import java.util.HashMap;

public class MapManager {

    public static ArrayList<Integer> maps = new ArrayList<>();

    public static ArrayList<Integer> availibeMap = new ArrayList<>();

    public static void init(){
        for (int i = 1; i <= MapsConfig.getMapsAmount(); i++) {
            maps.add(i);
            availibeMap.add(i);
        }
    }

}
