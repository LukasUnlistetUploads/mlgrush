package de.primeapi.mlgrush.managers;


import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.RushPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class QueueManager {

    public static HashMap<UUID, UUID> requests = new HashMap<>();

    public static ArrayList<UUID> queue = new ArrayList<>();


    public static String getSentRequest(RushPlayer p){
        if(queue.contains(p.getUuid())){
            return "Warteschlange";
        }
        if(requests.containsKey(p.getUuid())){
            return MLGRush.getInstance().getPlayer(requests.get(p.getUuid())).getName();
        }
        return "-";
    }

    public static List<String> getResievedRequests(RushPlayer p){
        List<String> list = new ArrayList<>();
        if(!requests.containsValue(p.getUuid())){
            list.add("-");
        }else {
            for(UUID uuid : requests.keySet()){
                if(requests.get(uuid).equals(p.getUuid())){
                    list.add(MLGRush.getInstance().getPlayer(uuid).getName());
                }
            }
        }
        return list;
    }


}
