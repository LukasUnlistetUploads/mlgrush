package de.primeapi.mlgrush.managers;

import de.primeapi.mlgrush.MLGRush;
import de.primeapi.mlgrush.api.Duels;
import de.primeapi.mlgrush.api.RushPlayer;
import de.primeapi.mlgrush.config.SettingsConfig;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class ScoreboardManager {

    public static void sendLobby(@Nonnull RushPlayer p){
        if(!p.isLobby()){
            sendIngame(p);
            return;
        }
        Scoreboard scoreboard = new net.minecraft.server.v1_8_R3.Scoreboard();
        ScoreboardObjective obj = scoreboard.registerObjective("zagd", IScoreboardCriteria.b);
        obj.setDisplayName( ChatColor.translateAlternateColorCodes('&', SettingsConfig.cfg.getString("settings.scoreboard.lobby.title")));

        PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(obj, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);
        PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(obj, 1);

        sendPacket(removePacket, p.thePlayer());
        sendPacket(createPacket, p.thePlayer());
        sendPacket(display, p.thePlayer());

        List<String> list = new ArrayList<>();

        for(String s : SettingsConfig.cfg.getStringList("settings.scoreboard.lobby.content")){
            s = s.replaceAll("%rank%", String.valueOf(p.getRank()));
            s = s.replaceAll("%sent%", QueueManager.getSentRequest(p));
            if(s.equals("%REQUESTS%")){
                for(String s1 : QueueManager.getResievedRequests(p)){
                    list.add(ChatColor.translateAlternateColorCodes('&',SettingsConfig.cfg.getString("settings.scoreboard.lobby.request").replaceAll("%name%", s1)));
                }
                continue;
            }
            list.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        int i = list.size();
        for(String s : list){
            ScoreboardScore score = new ScoreboardScore(scoreboard, obj, s);
            score.setScore(i);
            PacketPlayOutScoreboardScore pa = new PacketPlayOutScoreboardScore(score);
            sendPacket(pa, p.thePlayer());
            i--;
        }
    }

    public static void sendIngame(@Nonnull RushPlayer p){
        if(p.isLobby()){
            sendLobby(p);
            return;
        }

        Scoreboard scoreboard = new net.minecraft.server.v1_8_R3.Scoreboard();
        ScoreboardObjective obj = scoreboard.registerObjective("zagd", IScoreboardCriteria.b);
        obj.setDisplayName( ChatColor.translateAlternateColorCodes('&', SettingsConfig.cfg.getString("settings.scoreboard.ingame.title")));

        PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(obj, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);
        PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(obj, 1);

        sendPacket(removePacket, p.thePlayer());
        sendPacket(createPacket, p.thePlayer());
        sendPacket(display, p.thePlayer());

        List<String> list = new ArrayList<>();
        int points;
        if(Duels.getBluePlayer(p.getUuid()).equals(p.getUuid())){
            points = Duels.getBlueScore(p.getUuid());
        }else {
            points = Duels.getRedScore(p.getUuid());
        }
        int pointso;
        if(Duels.getBluePlayer(p.getUuid()).equals(Duels.getOpponent(p.getUuid()))){
            pointso = Duels.getBlueScore(p.getUuid());
        }else {
            pointso = Duels.getRedScore(p.getUuid());
        }

        for(String s : SettingsConfig.cfg.getStringList("settings.scoreboard.ingame.content")){
            s = s.replaceAll("%opponent%", MLGRush.getInstance().getPlayer(Duels.getOpponent(p.getUuid())).getName());
            s = s.replaceAll("%points_own%", String.valueOf(points));
            s = s.replaceAll("%point_opponent%", String.valueOf(pointso));
            list.add(ChatColor.translateAlternateColorCodes('&', s));
        }

        int i = list.size();
        for(String s : list){
            ScoreboardScore score = new ScoreboardScore(scoreboard, obj, s);
            score.setScore(i);
            PacketPlayOutScoreboardScore pa = new PacketPlayOutScoreboardScore(score);
            sendPacket(pa, p.thePlayer());
            i--;
        }
    }






    private static void sendPacket(@SuppressWarnings("rawtypes") Packet packet, Player p) {
        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
    }

}
