package de.primeapi.mlgrush.managers.messages;

import lombok.Getter;
import lombok.Setter;
@Getter
public enum Message {

    NOPERMS("§c Keine Berechtigung!", true),

    SETUP_0("§7 Begib dich zum Spawn und tippe §8'§efertig§8'", true),
    SETUP_1("§7 Begib dich zum Spawn von Team §cRot §7und tippe §8'§efertig§8'", true),
    SETUP_2("§7 Begib dich zum Spawn von Team §1Blau §7und tippe §8'§efertig§8'", true),
    SETUP_3("§7 Zerschlage die untere Hälfte des §eBettes §7von §cTeam Rot", true),
    SETUP_4("§7 Zerschlage die obere Hälfte des §eBettes §7von §cTeam Rot", true),
    SETUP_5("§7 Zerschlage die untere Hälfte des §eBettes §7von §1Team Blau", true),
    SETUP_6("§7 Zerschlage die obere Hälfte des §eBettes §7von §1Team Blau", true),
    SETUP_7_1("§7 Begib dich zum 1. Punkt der Arena", true),
    SETUP_7_2("§c Hinweis: §7Es muss sich um den nord-westliche unteren Punkt handel!", true),
    SETUP_8_1("§7 Begib dich zum 2. Punkt der Arena", true),
    SETUP_8_2("§c Hinweis: §7Es muss sich um den süd-östlichen oberen Punkt handel! (Diese gibt das Baulimit an!)", true),
    SETUP_9("§7 Speichere nun den Server und tippe §8'§efertig§8'", true),

    TABCOMPLETE_NOPLAYERS("§7 Es spielen aktuell keine Spieler!", true),

    CHALLANGE_QUEUE_LEAVE("§7 Du hast die Warteschlange §cverlassen§7!", true),
    CHALLANGE_CHALLANGE_BACK_NOTIFY("§e %player%§7 hat die Herausforderung §czurückgezogen§7!", true),
    CHALLANGE_CHALLANGE_BACK_SUCCESS("§7 Du hast die Anfrage an §e%player%§c zurückgezogen§7!", true),
    CHALLANGE_QUEUE_ENTER("§7 Du hast die Warteschlange §abetreten§7!", true),
    CHALLANGE_NOTFOUND("§c Dieser Spieler ist nicht online!", true),
    CHALLANGE_ALREADY_MATCH("§c Du befinest dich bereits in einem Match!", true),
    CHALLANGE_ALREAY_OTHER("§c Dieser Spieler befindet sich aktuell in einem Match!", true),
    CHALLANGE_SELF("§7 Du kannst dich nicht selbst herausfordern!", true),
    CHALLANGE_QUEUE_("", true),
    CHALLANGE_CHALLANGE_START_SUCCESS("§7 Du hast §e%player%§a erfolgreich §7zu einem Duell herausgefordert!", true),
    CHALLANGE_CHALLANGE_START_NOTIFY("§7 Du wurdest von §e%player%§7 zu einem Duell herausgefordert! ", true),
    CHALLANGE_USAGE("§7 Benutze: §e/c <Spieler>", true),
    CHALLANGE_DEACTIVE("§c Dieser Spieler erhält keine Herrausforderungen", true),

    QUIT_NOMATCH("§7 Du bist in keinem Duell!", true),

    DUELL_WINNER(" §7Du hast das §cDuell §7gegen §e%player%§a gewonnen§7!", true),
    DUELL_LOOSER(" §7Du hast das §cDuell §7gegen §e%player%§c verloren§7!", true),
    DUELL_END("§7 Das Duell ist beendet!", true),
    DUELL_START_1("§7 Das Spiel gegen §e%player%§7 startet!", true),
    DUELL_START_2("§7 Beende das Spiel mit §e/quit", true),
    DUELL_LEAVE_NOTIFY("§e%player%§7 hat den Server verlassen!", true),
    DUELL_("", true),

    SETSTATSWALL_USAGE("§7 Benutze: §e§l/setstatswall <position> <SIGN/HEAD>", true),
    SETSTATSWALL_NOHEAD("§c Du musst auf ein Kopf gucken!", true),
    SETSTATSWALL_NOSIGN("§c Du musst auf ein Schild gucken!", true),
    SETSTATSWALL_NOBLOCK("§c Du guckst auf keinen Block!", true),
    SETSTATSWALL_NONUMBER("§c Das 1. Argument muss eine Zahl sein!", true),

    SETUP_USAGE_1("§7 Bitte bestätige mit /setup confirm", true),
    SETUP_USAGE_2("§4 Warnung: §7Alle spieler werden gekickt!", true),

    SPEC_LEAVE("§7 Du hast den Spec-Modus verlassen!", true),
    SPEC_USAGE("§7 Benutze: §e/spec <name>", true),
    SPEC_NOTFOUND("§c Dieser Spieler ist nicht online!", true),
    SPEC_NOMATCH("§7 Dieser Spieler ist in keinem Spiel!", true),
    SPEC_SUCCESS("§7 Du beobachtest nun §e%player%§7!", true),

    STATS_RESET_USAGE("§7 Benutze: §e/stats reset <Spieler>", true),
    STATS_RESET_NOTFOUND("§c Dieser Spieler existiert nicht!", true),
    STATS_RESET_SUCCESS("§a Du hast erfolgreich die §eStats §7von §e%player% §czurückgesetzt§7!", true),
    STATS_NOTFOUND("§7 Dieser Spieler ist dem System nicht bekannt!", true),
    STATS_1(" §7§m-----------§r §6Stats §7§m-----------", true),
    STATS_2(" §6Rank§f: #%count%", true),
    STATS_3(" §6Kills§f: %count%", true),
    STATS_4(" §6Deaths§f: %count%", true),
    STATS_5(" §6K/D§f: %count%", true),
    STATS_6(" §6Wins§f: %count%", true),
    STATS_7(" §6Looses§f: %count%", true),
    STATS_8(" §6W/L§f: %count%", true),
    STATS_9(" §6Betten§f: %count%", true),
    STATS_10(" §7§m-----------§r §6Stats §7§m-----------", true),

    BEDS_OWN("§c Du kannst dein eigenes Bett nicht abbauen!", true),
    BEDS_DESTROY_RED("§e %player%§7 hat das Bett von §cTeam Rot §czerstört§7!", true),
    BEDS_DETROY_BLUE("§e %player%§7 hat das Bett von §9Team Blau §czerstört§7!", true),

    INVSORT_SAVE("§a Deine Einstellungen wurden erfolgreich gespeichert!", true),

    TOGGLEINVITES_ON("§7 Du §aerhälst nun §7wieder §eHerausvorderungen§7!", true),
    TOGGLEINVITES_OFF("§7 Du §cerhälst nun keine §eHerausvorderungen§7 mehr!", true),

    OTHER_ITEMS_CHALLANGE("§7➠ §6Spieler herausfordern §7×", false),
    OTHER_ITEMS_SETTINGS("§7➠ §6Einstellungen §7×", false),
    OTHER_ITEMS_LOBBY("§7➠ §6Zurück zur Lobby §7×", false),

    OTHER_QUEUE_NAME("§7➠ §6Warteschlange §7×", false),

    OTHER_SORT_TTTLE("§7➠ §6Inventar-Sortierung", false),
    OTHER_SORT_STICK("§7➠ §6Knüppel", false),
    OTHER_SORT_BLOCKS("§7➠ §6Blöcke", false),
    OTHER_SORT_PICKAXE("§7➠ §6Spitzhacke", false),

    SETTINGS_TITLE("§6Einstellungen", false),
    SETTINGS_INVENTORY_SORT("§eInventarsortierung", false),
    SETTINGS_ACCEPT_OFF("§cErlaube Herausvordeungen", false),
    SETTINGS_ACCEPT_ON("§aDeaktiviere Herausvorderungen", false),

    PLACEHOLDER("DO NOT REMOVE", true);

    String path;
    @Setter
    String content;
    Boolean prefix;

    Message(String content, Boolean prefix){
        this.content = content;
        this.prefix = prefix;
        this.path = this.toString().replaceAll("_", ".").toLowerCase();
    }
    public Message replace(String key, String value){
        if(!key.startsWith("%")){
            key = "%" + key + "%";
        }
        String s = getContent().replaceAll(key, value);
        PLACEHOLDER.setContent(s);
        return PLACEHOLDER;
    }

    public Message replace(String key, Object value){
        return replace(key, String.valueOf(value));
    }
}
