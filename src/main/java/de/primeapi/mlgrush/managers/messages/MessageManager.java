package de.primeapi.mlgrush.managers.messages;

import de.primeapi.mlgrush.MLGRush;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class MessageManager {


    File file;
    YamlConfiguration cfg;


    public MessageManager() throws IOException {
        file = new File("plugins/mlgrush", "messages.yml");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        cfg = YamlConfiguration.loadConfiguration(file);

        MLGRush.getInstance().getThreadPoolExecutor().submit(() -> {
            int i = 0;
            for (Message message : Message.values()) {
                if (cfg.contains(message.getPath())) {
                    message.setContent(ChatColor.translateAlternateColorCodes('&', cfg.getString(message.getPath())).replaceAll("%prefix%", MLGRush.getInstance().getPrefix()));
                } else {
                    String s = (message.getPrefix() ? "%prefix%" : "") + message.getContent().replaceAll("§", "&");
                    cfg.set(message.getPath(), s);
                    i++;
                    message.setContent(ChatColor.translateAlternateColorCodes('&', s.replaceAll("%prefix%", MLGRush.getInstance().getPrefix())));
                }
            }
            System.out.println("[BungeeSystem] Es wurde(n) " + i + " neue Nachricht(en) in die messages.yml eingefügt!");
            try {
                cfg.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

}
