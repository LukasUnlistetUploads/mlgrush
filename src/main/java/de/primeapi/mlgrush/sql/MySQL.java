package de.primeapi.mlgrush.sql;

import de.primeapi.mlgrush.MLGRush;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MySQL {

    public static Connection c;

    public static void connect(String host, String database, String username, String password) {
        try {
            c = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database + "?autoReconnect=true",username, password);
            System.out.println("[MLGRUSH] MySQL-Verbindung aufgebaut.");
        } catch (SQLException e) {
            System.out.println("[MLGRUSH] MySQL-Verbindung fehlgeschlagen: " + e.getMessage());
        }
    }

    public static void disconnect() {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void update(String qry) {
        try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate(qry);
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void setup(){
        MLGRush.getInstance().getThreadPoolExecutor().submit(() -> {
            try {
                MySQL.c.prepareStatement("CREATE TABLE IF NOT EXISTS players(name varchar(100),realname varchar(100),uuid varchar(100),wins int,looses int,rounds int,kills int,deaths int,stick int,pickaxe int,blocks int, beds int,invites boolean)").execute();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
    }

    public static ArrayList<UUID> getAllPlayers(){
        ArrayList<UUID> list = new ArrayList<>();
        try {
            PreparedStatement st = c.prepareStatement("SELECT * FROM players ORDER BY wins DESC");
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(UUID.fromString(rs.getString("uuid")));
            }
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

}
