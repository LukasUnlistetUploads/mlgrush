package de.primeapi.mlgrush.sql;


import com.sun.org.apache.xpath.internal.operations.Bool;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class SQLPlayer {

    private String name;
    private UUID uuid;

    public SQLPlayer(String name, UUID uuid) {
        this.name = name.toLowerCase();
        this.uuid = uuid;
        if(!exists("uuid", uuid)) insert();
        setName(name.toLowerCase());
    }

    public SQLPlayer(String name){
        this.name = name.toLowerCase();
        if(!exists("name", name.toLowerCase())) throw new IllegalArgumentException("Spieler existiert nicht!");
        setup();
    }

    public SQLPlayer(UUID uuid){
        this.uuid = uuid;
        if(!exists("uuid", uuid)) throw new IllegalArgumentException("Spieler existiert nicht!");
        setup();

    }

    public boolean exists(String identifier, Object value){
        boolean b = false;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE " + identifier + "=?");
            if (value instanceof String) {
                st.setString(1, String.valueOf(value));
            } else if (value instanceof UUID) {
                st.setString(1, value.toString());
            } else {
                if (value instanceof Long || value instanceof Integer) {
                    st.setInt(1, (Integer) value);
                }
            }

            ResultSet rs = st.executeQuery();
            b = rs.next();
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return b;
    }

    public void insert(){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("INSERT INTO players values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            st.setString(1, name.toLowerCase());
            st.setString(2, name);
            st.setString(3, uuid.toString());
            st.setInt(4, 0);
            st.setInt(5, 0);
            st.setInt(6, 0);
            st.setInt(7, 0);
            st.setInt(8, 0);
            st.setInt(9, 0);
            st.setInt(10, 1);
            st.setInt(11, 2);
            st.setInt(12, 0);
            st.setBoolean(13, true);
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void setup(){
        if(Objects.nonNull(this.uuid)) {
            name = getString("name");
        } else if (Objects.nonNull(this.name)) {
            try {
                PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE name=?;");
                st.setString(1, name);

                ResultSet rs = st.executeQuery();
                if(rs.next()) this.uuid = UUID.fromString(rs.getString("uuid"));
                rs.close();
                st.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }


    private String getString(String key){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE uuid=?;");
            st.setString(1, uuid.toString());

            ResultSet rs = st.executeQuery();
            if(rs.next()) s = rs.getString(key);
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return s;
    }
    private Integer getInteger(String key){
        int i = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE uuid=?;");
            st.setString(1, uuid.toString());

            ResultSet rs = st.executeQuery();
            if(rs.next()) i = rs.getInt(key);
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return i;
    }
    private Long getLong(String key){
        long l = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE uuid=?;");
            st.setString(1, uuid.toString());

            ResultSet rs = st.executeQuery();
            if(rs.next()) l = rs.getLong(key);
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return l;
    }
    private Boolean getBoolean(String key){
        boolean b = false;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE uuid=?;");
            st.setString(1, uuid.toString());

            ResultSet rs = st.executeQuery();
            if(rs.next()) b = rs.getBoolean(key);
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return b;
    }


    public String getName() {
        return name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getRealname() {
        return getString("realname");
    }

    public int getDeaths() {
        return getInteger("deaths");
    }

    public int getKills() {
        return getInteger("kills");
    }

    public int getLooses() {
        return getInteger("looses");
    }

    public int getRounds() {
        return getInteger("rounds");
    }

    public int getWins() {
        return getInteger("wins");
    }

    public int getBlocks() {
        return getInteger("blocks");
    }

    public int getPickaxe() {
        return getInteger("pickaxe");
    }

    public int getStick() {
        return getInteger("stick");
    }

    public int getBeds() {
        return getInteger("beds");
    }

    public boolean getInvites() {
        return getBoolean("invites");
    }

    public void setName(String name){
        MySQL.update("UPDATE players SET name='" + name + "' WHERE UUID='" + uuid + "';");
         
    }
    public void setRealname(String realname){
        MySQL.update("UPDATE players SET realname='" + realname + "' WHERE UUID='" + uuid + "';");

    }

    public void setDeaths(int deaths) {
        MySQL.update("UPDATE players SET deaths=" + deaths + " WHERE UUID='" + uuid + "';");
         
    }

    public void setKills(int kills) {
        MySQL.update("UPDATE players SET kills=" + kills + " WHERE UUID='" + uuid + "';");
         
    }

    public void setLooses(int looses) {
        MySQL.update("UPDATE players SET looses=" + looses + " WHERE UUID='" + uuid + "';");
         
    }

    public void setRounds(int rounds) {
        MySQL.update("UPDATE players SET rounds=" + rounds + " WHERE UUID='" + uuid + "';");
         
    }

    public void setWins(int wins) {
        MySQL.update("UPDATE players SET wins=" + wins + " WHERE UUID='" + uuid + "';");
         
    }

    public void setStick(int stick) {
        MySQL.update("UPDATE players SET stick=" + stick + " WHERE UUID='" + uuid + "';");
         
    }

    public void setPickaxe(int pickaxe) {
        MySQL.update("UPDATE players SET pickaxe=" + pickaxe + " WHERE UUID='" + uuid + "';");
         
    }

    public void setBlocks(int blocks) {
        MySQL.update("UPDATE players SET blocks=" + blocks + " WHERE UUID='" + uuid + "';");

    }

    public void setBeds(int blocks) {
        MySQL.update("UPDATE players SET beds=" + blocks + " WHERE UUID='" + uuid + "';");

    }

    public void setInvites(boolean invites) {
        MySQL.update("UPDATE players SET invites=" + invites + " WHERE UUID='" + uuid + "';");

    }

    public void addWins(int i){
        setWins(getWins() + i);
    }

    public void addLosses(int i){
        setLooses(getLooses() + i);
    }

    public void addRounds(int i){
        setRounds(getRounds() + i);
    }

    public void addKills(int i){
        setKills(getKills() + i);
    }

    public void addBeds(int i){
        setBeds(getBeds() + i);
    }

    public void addDeaths(int i){
        setDeaths(getDeaths() + i);
    }

    public int getRank() {
        List<UUID> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players ORDER BY wins DESC");
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(UUID.fromString(rs.getString("uuid")));
            }
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        for(UUID uuid : list){
            if(uuid.equals(getUuid())){
                return list.indexOf(uuid) + 1;
            }
        }
        return -1;
    }

    public double getKD(){
        double kills = getKills();
        double deaths = getDeaths();
        if(kills == 0){
            return 0;
        }
        if(deaths == 0 || deaths == 1){
            return kills;
        }
        double kd = kills / deaths;
        return Math.round(kd * 100D) / 100D;
    }

    public double getWL(){
        double wins = getWins();
        double deaths = getDeaths();
        if(wins == 0){
            return 0.00;
        }
        if(deaths == 0 || deaths == 1){
            return wins;
        }
        double wl = wins / deaths;
        return Math.round(wl * 100D) / 100D;
    }
}
