package de.primeapi.mlgrush.utilities;

import de.primeapi.mlgrush.managers.messages.Message;

public class Names {

    public static String HOTBAR_SWORD = Message.OTHER_ITEMS_CHALLANGE.getContent();
    public static String HOTBAR_SETTINGS = Message.OTHER_ITEMS_SETTINGS.getContent();
    public static String HOTBAR_LEAVE = Message.OTHER_ITEMS_LOBBY.getContent();

    public static String WITCH_NAME = Message.OTHER_QUEUE_NAME.getContent();

    public static String SORT_TITLE = Message.OTHER_SORT_TTTLE.getContent();
    public static String SORT_STICK = Message.OTHER_SORT_STICK.getContent();
    public static String SORT_BLOCKS = Message.OTHER_SORT_BLOCKS.getContent();
    public static String SORT_PICKAXE = Message.OTHER_SORT_PICKAXE.getContent();


}
