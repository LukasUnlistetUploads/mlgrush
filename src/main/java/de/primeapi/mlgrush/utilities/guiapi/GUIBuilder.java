package de.primeapi.mlgrush.utilities.guiapi;

import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.HashMap;

public class GUIBuilder {

    public Inventory inventory;
    public HashMap<ItemStack, IClickAction> items = new HashMap<>();
    public ICloseAction closeAction;


    public GUIBuilder(@NonNull Inventory inventory) {
        this.inventory = inventory;
    }

    public GUIBuilder(@NonNull int size) {
        this.inventory = Bukkit.createInventory(null, size, null);
    }

    public GUIBuilder(@NonNull int size, String title) {
        this.inventory = Bukkit.createInventory(null, size, title);
    }

    public GUIBuilder(@Nullable InventoryHolder inventoryHolder, @NonNull int size) {
        this.inventory = Bukkit.createInventory(null, size, null);
    }

    public GUIBuilder(@Nullable InventoryHolder inventoryHolder, @NonNull int size, @Nullable String title) {
        this.inventory = Bukkit.createInventory(null, size, title);
    }

    public GUIBuilder(@Nullable InventoryHolder inventoryHolder, @NonNull InventoryType inventoryType, @NonNull String title) {
        this.inventory = Bukkit.createInventory(null, inventoryType, title);
    }


    public GUIBuilder fillInventory(ItemStack itemStack) {
        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, itemStack);
        }
        return this;
    }

    public GUIBuilder fillInventory(ItemStack itemStack, IClickAction clickAction) {
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack item = itemStack.clone();
            inventory.setItem(i, item);
            items.put(item, clickAction);
        }
        return this;
    }

    public GUIBuilder addItem(int slot, ItemStack itemStack, IClickAction clickAction) {
        items.put(itemStack, clickAction);
        addItem(slot, itemStack);
        return this;
    }

    public GUIBuilder addItem(int slot, ItemStack itemStack) {
        inventory.setItem(slot, itemStack);
        return this;
    }

    public GUIBuilder addCloseAction(ICloseAction closeAction) {
        this.closeAction = closeAction;
        return this;
    }

    public Inventory build() {
        GUIManager.registerGUI(this);
        return inventory;
    }

    public interface IClickAction {

        void onClick(Player p, ItemStack itemStack);

    }
    public interface ICloseAction {

        void onClose(Player p, Inventory inventory);
    }



}

