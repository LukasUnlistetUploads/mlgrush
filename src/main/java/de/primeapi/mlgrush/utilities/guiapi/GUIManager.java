package de.primeapi.mlgrush.utilities.guiapi;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Objects;

public class GUIManager implements Listener {

    public static HashMap<ItemStack, GUIBuilder.IClickAction> items = new HashMap<>();
    public static HashMap<Inventory, GUIBuilder.ICloseAction> inventories = new HashMap<>();


    public static void registerGUI(GUIBuilder builder) {
        items.putAll(builder.items);
        inventories.put(builder.inventory, builder.closeAction);
    }

    @EventHandler
    public void inInventoryClick(InventoryClickEvent e) {
        if (inventories.containsKey(e.getInventory())) {
            e.setCancelled(true);
        }
        if (items.containsKey(e.getCurrentItem())) {
            if (e.getWhoClicked() instanceof Player) {
                e.setCancelled(true);
                items.get(e.getCurrentItem()).onClick((Player) e.getWhoClicked(), e.getCurrentItem());
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (inventories.containsKey(e.getInventory())) {
            if (e.getPlayer() instanceof Player) {
                if (Objects.nonNull(inventories.get(e.getInventory())))
                    inventories.get(e.getInventory()).onClose((Player) e.getPlayer(), e.getInventory());
            }
        }
    }


}
